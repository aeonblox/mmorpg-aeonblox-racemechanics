package com.aeonblox.RaceMechanics;

import org.bukkit.ChatColor;

public enum BonusRaceType {
	Paladin("paladin",ChatColor.RESET + ""+ ChatColor.BLUE+ "", ChatColor.DARK_AQUA + "" + ChatColor.BOLD + " [P]" + ChatColor.RESET + ""),
	Angel("angel", ChatColor.WHITE+ "", ChatColor.YELLOW + ""+ ChatColor.BOLD + " [A]" + ChatColor.RESET),
	Assassin("assassin", ChatColor.BLACK + "", ChatColor.BLACK + "" + ChatColor.BOLD +" [A]" + ChatColor.RESET),
	None("none", ChatColor.RESET + "", ChatColor.RESET + "");
	private BonusRaceType(String name, String chatColor, String suffix){
		this.name = name;
		this.chatColor=chatColor;
		this.suffix = suffix;
	}
	private String chatColor;
	private String name;
	private String suffix;
	public String getNameColor(){
		return chatColor;
	}
	public String getSuffix(){
		return this.suffix;
	}
	public String getName(){
		return this.name;
	}
	public static BonusRaceType forName(String s){
		return RaceType.bonusForName(s);
	}
}
