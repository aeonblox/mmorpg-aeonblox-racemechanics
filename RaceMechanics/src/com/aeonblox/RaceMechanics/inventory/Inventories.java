package com.aeonblox.RaceMechanics.inventory;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
@SuppressWarnings("deprecation")
public class Inventories {
	public static void nullify(){
		inv = null;
		pane = null;
		pane_meta = null;
		red_pane = null;
		g_p = null;
		red_pane_meta = null;
		g_p_m = null;
		hash_b = null;
		hash_p = null;
		paladin_b = null;
		paladin_m = null;
		angel_b = null;
		angel_m = null;
		assassin_b = null;
		assassin_m = null;
		b_p = null;
		b_p_m = null;
		bl_p = null;
		bl_p_m = null;
		pl_p = null;
		p_p = null;
		pl_p_m = null;
		y_p = null;
		y_p_m = null;
		gl_p = null;
		gl_p_m = null;
		p_p = null;
		p_p_m = null;
	}
	public static Inventory inv;
	public static ItemStack pane = new ItemStack(Material.STAINED_GLASS_PANE, 1);
	private static ItemMeta pane_meta = pane.getItemMeta();
	public static ItemStack red_pane = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getData());
	private static ItemMeta red_pane_meta = red_pane.getItemMeta();
	public static ItemStack g_p = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GREEN.getData());
	private static ItemMeta g_p_m = g_p.getItemMeta();
	//Hasher
	public static ItemStack hash_b = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData());
	private static ItemMeta hash_p = hash_b.getItemMeta();
	// Donator Race
	public static ItemStack paladin_b = new ItemStack(Material.GOLD_BLOCK, 1);
	private static ItemMeta paladin_m = paladin_b.getItemMeta();
	public static ItemStack angel_b = new ItemStack(Material.IRON_BLOCK, 1);
	private static ItemMeta angel_m = angel_b.getItemMeta();
	public static ItemStack assassin_b = new ItemStack(Material.DIAMOND_BLOCK, 1);
	private static ItemMeta assassin_m = assassin_b.getItemMeta();
	// Is it DiamondBlock or Diamond_Block ( Doing this shizzle without eclipse )
	//
	public static ItemStack b_p = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.BLUE.getData());
	private static ItemMeta b_p_m = b_p.getItemMeta();
	public static ItemStack p_p = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.PURPLE.getData());
	private static ItemMeta p_p_m = p_p.getItemMeta();
	public static ItemStack bl_p = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.BLACK.getData());
	private static ItemMeta bl_p_m = bl_p.getItemMeta();
	public static ItemStack pl_p = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.PINK.getData());
	private static ItemMeta pl_p_m = pl_p.getItemMeta();
	public static ItemStack y_p = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.YELLOW.getData());
	private static ItemMeta y_p_m = y_p.getItemMeta();
	public static ItemStack gl_p = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.ORANGE.getData());
	private static ItemMeta gl_p_m = g_p.getItemMeta();
	public static void createChooseMenu(){
		inv = Bukkit.createInventory(null, 54, ChatColor.GOLD + "Choose your race!");
		gl_p_m.setDisplayName(ChatColor.GOLD + "Titan");
		gl_p_m.setLore(createLore(ChatColor.GOLD + "If you click this pane, you will be turned into a Titan"));
		gl_p.setItemMeta(gl_p_m);
		pane_meta.setDisplayName(ChatColor.GRAY + "#####");
		pane.setItemMeta(pane_meta);
		red_pane_meta.setDisplayName(ChatColor.RED + "Demon");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.RED + "If you click this pane, you will be turned into a Demon!");
		red_pane_meta.setLore(lore);
		red_pane.setItemMeta(red_pane_meta);
		g_p_m.setDisplayName(ChatColor.GREEN + "Elf");
		g_p_m.setLore(createLore(ChatColor.GREEN + "If you click this pane, you will be turned into an elf!"));
		g_p.setItemMeta(g_p_m);
		b_p_m.setDisplayName(ChatColor.AQUA + "Nymph");
		b_p_m.setLore(createLore(ChatColor.AQUA + "If you click this pane, you will be turned into a nymph!"));
		b_p.setItemMeta(b_p_m);
		pl_p_m.setDisplayName(ChatColor.LIGHT_PURPLE + "Human");
		pl_p_m.setLore(createLore(ChatColor.LIGHT_PURPLE + "If you click this pane, you will be turned into a Human!"));
		pl_p.setItemMeta(pl_p_m);
		bl_p_m.setDisplayName(ChatColor.GRAY + "Wraith");
		bl_p_m.setLore(createLore(ChatColor.GRAY + "If you click this pane, you will be turned into a Wraith!"));
		bl_p.setItemMeta(bl_p_m);
		p_p_m.setDisplayName(ChatColor.DARK_PURPLE + "Chameleon");
		p_p_m.setLore(createLore(ChatColor.DARK_PURPLE + "If you click this pane, you will be turned into a Chameleon!"));
		p_p.setItemMeta(p_p_m);
		y_p_m.setDisplayName(ChatColor.YELLOW + "Priest");
		y_p_m.setLore(createLore(ChatColor.YELLOW + "If you click this pane, you will be turned into a Priest!"));
		y_p.setItemMeta(y_p_m);
		hash_p.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Donator Races:");
		hash_p.setLore(createLore(ChatColor.YELLOW + "Past here, are the donator races, you must pay for access to them."));
		hash_b.setItemMeta(hash_p);
		paladin_m.setDisplayName(ChatColor.GOLD + "Paladin");
		paladin_m.setLore(createLore(ChatColor.YELLOW + "If you click this pane, you will be turned into a Paladin!"));
		paladin_b.setItemMeta(paladin_m);
		angel_m.setDisplayName("Angel");
		angel_m.setLore(createLore("If you click this pane, you will be turned into a Angel!"));
		angel_b.setItemMeta(angel_m);
		assassin_m.setDisplayName(ChatColor.BLACK + "Assassin");
		assassin_m.setLore(createLore(ChatColor.BLACK + "If you click this pane, you will be turned into a Assassin!"));
		assassin_b.setItemMeta(assassin_m);

		
	}
	public Inventory chooseMenu(){
		return inv;
	}
	private static List<String> createLore(String s){
		List<String> lore = new ArrayList<String>();
		lore.add(s);
		return lore;
	}
}
