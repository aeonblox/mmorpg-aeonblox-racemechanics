package com.aeonblox.RaceMechanics;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.aeonblox.RaceMechanics.MySQL.MySQL;
import com.aeonblox.RaceMechanics.commands.MainCommand;
import com.aeonblox.RaceMechanics.commands.NinjaAndNormal;
import com.aeonblox.RaceMechanics.commands.PBrightCommand;
import com.aeonblox.RaceMechanics.commands.PFlyCommand;
import com.aeonblox.RaceMechanics.commands.PfireCommand;
import com.aeonblox.RaceMechanics.commands.PhealCommand;
import com.aeonblox.RaceMechanics.commands.PnearCommand;
import com.aeonblox.RaceMechanics.config.Config;
import com.aeonblox.RaceMechanics.config.CustomConfig;
import com.aeonblox.RaceMechanics.events.clickAndBreak.AirClickEvent;
import com.aeonblox.RaceMechanics.events.clickAndBreak.BlockBreak;
import com.aeonblox.RaceMechanics.events.clickAndBreak.BowPullBackEvent;
import com.aeonblox.RaceMechanics.events.clickAndBreak.EntityInteract;
import com.aeonblox.RaceMechanics.events.clickAndBreak.RacePaneClick;
import com.aeonblox.RaceMechanics.events.inventory.EnchantPeekListener;
import com.aeonblox.RaceMechanics.events.inventory.InventoryClick;
import com.aeonblox.RaceMechanics.events.inventory.ItemPickUp;
import com.aeonblox.RaceMechanics.events.player.BowHitEvent;
import com.aeonblox.RaceMechanics.events.player.FireDamage;
import com.aeonblox.RaceMechanics.events.player.JoinEvent;
import com.aeonblox.RaceMechanics.events.player.LeaveEvent;
import com.aeonblox.RaceMechanics.events.player.PlayerDamage;
import com.aeonblox.RaceMechanics.events.player.PlayerMoveEventListener;
import com.aeonblox.RaceMechanics.events.player.RespawnEvent;
import com.aeonblox.RaceMechanics.events.player.ToggleSneakEvent;
import com.aeonblox.RaceMechanics.inventory.Inventories;
import com.aeonblox.RaceMechanics.runnables.PhealAndPflyRunnable;
import com.aeonblox.RaceMechanics.runnables.WaterDmgRunnable;
import com.aeonblox.RaceMechanics.utils.MySQLUtils;
import com.aeonblox.RaceMechanics.utils.RacePlayer;
import com.aeonblox.RaceMechanics.utils.Utils;

public class RMMain extends JavaPlugin{

	private String prefix = ChatColor.YELLOW + "[" + ChatColor.GREEN + "RM" + ChatColor.YELLOW + "]: ";
	private HashMap<String, String> messages = new HashMap<String, String>();
	public Config config = new Config("config");
	public ArrayList<Material> noVineBlocks = new ArrayList<Material>();
	public ArrayList<UUID> climbingPlayers = new ArrayList<UUID>();
	public Config RacePlayerConfig = new Config("RacePlayers");
	public ArrayList<RacePlayer> RacePlayers = new ArrayList<RacePlayer>();
	public CustomConfig configHandler;
	public MySQLUtils mutils;
	public Utils utils;
	public Connection con;
	public MySQL mySQL = null;
	public String ip, port, username, password, database;
	public Inventories invs;
	public ArrayList<RacePlayer> pfirePlayers = new ArrayList<RacePlayer>();
	public boolean mysqlenabled = false;
	PBrightCommand pbright;


	public Utils getUtils(){
		return utils;
	}
	public void onEnable(){
		defineNoVineBlocks();
		PluginManager pm = getServer().getPluginManager();
		configHandler = new CustomConfig(this);
		this.mutils = new MySQLUtils(this);
		utils = new Utils(this,mutils);
		invs = new Inventories();
		defineMessages();
		pbright = new PBrightCommand(this);
		if (config.file == null || config.fileConfig == null){
			configHandler.saveDefaultConfig(config);
		}
		this.mysqlenabled = Boolean.valueOf(configHandler.getCustomConfig(config).getString("mysql-enabled"));
		if (RacePlayerConfig.file == null || RacePlayerConfig.fileConfig == null){
			configHandler.saveDefaultConfig(RacePlayerConfig);
		}
		Logger log = this.getLogger();
		if (mysqlenabled) {
			long currentTime = System.currentTimeMillis();
			this.username = configHandler.getCustomConfig(config).getString("mysql-username");
			this.password = configHandler.getCustomConfig(config).getString("mysql-password");
			this.ip = configHandler.getCustomConfig(config).getString("mysql-host");
			this.database = configHandler.getCustomConfig(config).getString("mysql-database");
			this.port =  configHandler.getCustomConfig(config).getString("mysql-port");
			log.info("Attempting to connect to MySQL server with the following details: port: " + port + ", ip: " + ip + ", username: " + username + ", password: " + password);
			this.mySQL = new MySQL(this, ip, port,database, username, password);
			this.con = 	this.mySQL.openConnection();
			log.info("Attempting to create table racePlayers if it doesn't exist....");
			mySQL.updateSQL("CREATE TABLE IF NOT EXISTS racePlayers(uuid TEXT, BonusRaceType TEXT, RaceType TEXT, TitanType TEXT)");
			mySQL.closeConnection();
			try {
				this.con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			long currentTime2 = System.currentTimeMillis();
			long s = currentTime2 -currentTime;
			log.info("Finished MySQL setup in " + s + " miliseconds, or " + s/20 + " ticks");
		}
		Bukkit.getScheduler().runTaskLater(this, new Runnable() { public void run(){utils.getRacePlayers();}}, 10L);
		RaceType.putTypesInThing();
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new PhealAndPflyRunnable(this), 20L, 20L);
		pm.registerEvents(new JoinEvent(this, utils), this);
		pm.registerEvents(new EntityInteract(utils, this), this);
		//pm.registerEvents(new EntityTargetPlayerEvent(utils),this);
		pm.registerEvents(new PlayerDamage(utils), this);
		pm.registerEvents(new LeaveEvent(this, utils), this);
		pm.registerEvents(new InventoryClick(this, invs, utils), this);
		//pm.registerEvents(new ChatEvent(utils), this);
		pm.registerEvents(new FireDamage(this, utils), this);
		pm.registerEvents(new AirClickEvent(this, utils), this);
		//pm.registerEvents(new FoodLevelChange(utils), this);
		pm.registerEvents(new BowHitEvent(utils), this);
		pm.registerEvents(new EnchantPeekListener(this, utils), this);
		pm.registerEvents(new BowPullBackEvent(utils, this), this);
		//pm.registerEvents(new WaterEvent(utils), this);
		pm.registerEvents(new ToggleSneakEvent(utils), this);
		pm.registerEvents(new ItemPickUp(this, utils), this);
		//pm.registerEvents(new RespawnEvent(utils), this);
		pm.registerEvents(new PlayerMoveEventListener(this, utils), this);
		pm.registerEvents(new RacePaneClick(utils), this);
		pm.registerEvents(new BlockBreak(utils), this);
		//i thougpm.registerEvents(new ArmorEquipEvent(utils), this);
		WaterDmgRunnable wtrdmg = new WaterDmgRunnable(utils);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, wtrdmg, 20L, 20L);
		this.getCommand("AeonRace").setExecutor(new MainCommand(this, utils, invs));
		this.getCommand("pfly").setExecutor(new PFlyCommand(this));
		this.getCommand("pfire").setExecutor(new PfireCommand(this, utils));
		this.getCommand("pbright").setExecutor(pbright);
		this.getCommand("pheal").setExecutor(new PhealCommand(this));
		//this.getCommand("phidetag").setExecutor(new PhidetagCommand(utils));
		this.getCommand("normal").setExecutor(new NinjaAndNormal(this));
		this.getCommand("pnear").setExecutor(new PnearCommand(this));
		getLogger().info("Enabled!");
		Inventories.createChooseMenu();
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			public void run(){
				long then = System.currentTimeMillis();
				Bukkit.broadcastMessage(ChatColor.DARK_AQUA + "Saving all RacePlayers to the MySQL database..");
				utils.saveRacePlayers();
				long now = System.currentTimeMillis();
				long total = now - then;
				Bukkit.broadcastMessage(ChatColor.DARK_AQUA + "Done saving the RacePlayers to the MySQL database. It took approximately " + total + " milliseconds, or " + total/50 + " server ticks.");
			}
		}, 20L, 18000L);
	}
	public void onDisable(){
		utils.saveRacePlayers(true);
		PBrightCommand.revertFullbright();
		RacePlayers = null;
		pfirePlayers = null;
		RacePlayerConfig = null;
		configHandler = null;
		PhealCommand.phealOtherPlayers = null;
		PhealCommand.phealPlayers = null;
		Inventories.nullify();
	}
	public void defineNoVineBlocks()
	{
		noVineBlocks.add(Material.STEP);
		noVineBlocks.add(Material.THIN_GLASS);
		noVineBlocks.add(Material.WOOD_STAIRS);
		noVineBlocks.add(Material.JUNGLE_WOOD_STAIRS);
		noVineBlocks.add(Material.BIRCH_WOOD_STAIRS);
		noVineBlocks.add(Material.SPRUCE_WOOD_STAIRS);
		noVineBlocks.add(Material.COBBLESTONE_STAIRS);
		noVineBlocks.add(Material.BRICK_STAIRS);
		noVineBlocks.add(Material.WOOD_STAIRS);
		noVineBlocks.add(Material.SMOOTH_STAIRS);
		noVineBlocks.add(Material.NETHER_BRICK_STAIRS);
		noVineBlocks.add(Material.SANDSTONE_STAIRS);
		noVineBlocks.add(Material.FENCE);
		noVineBlocks.add(Material.FENCE_GATE);
		noVineBlocks.add(Material.NETHER_FENCE);
		noVineBlocks.add(Material.LADDER);
		noVineBlocks.add(Material.VINE);
		noVineBlocks.add(Material.BED);
		noVineBlocks.add(Material.BED_BLOCK);
		noVineBlocks.add(Material.IRON_FENCE);
		noVineBlocks.add(Material.SNOW);
		noVineBlocks.add(Material.SIGN);
		noVineBlocks.add(Material.LEVER);
		noVineBlocks.add(Material.TRAP_DOOR);
		noVineBlocks.add(Material.PISTON_EXTENSION);
		noVineBlocks.add(Material.PISTON_MOVING_PIECE);
		noVineBlocks.add(Material.TRIPWIRE_HOOK);
		noVineBlocks.add(Material.DIODE);
		noVineBlocks.add(Material.DIODE_BLOCK_OFF);
		noVineBlocks.add(Material.DIODE_BLOCK_OFF);
		noVineBlocks.add(Material.BOAT);
		noVineBlocks.add(Material.MINECART);
		noVineBlocks.add(Material.CAKE);
		noVineBlocks.add(Material.CAKE_BLOCK);
		noVineBlocks.add(Material.WATER);
		noVineBlocks.add(Material.STATIONARY_WATER);
		noVineBlocks.add(Material.LAVA);
		noVineBlocks.add(Material.STATIONARY_LAVA);
	}
	public void defineMessages(){
		addMsg("raceNotExists", ChatColor.RED + "That race does not exist!", true);
		addMsg("reloadedConfig", "You successfully reloaded the config!", true);
		addMsg("unknownCommand", ChatColor.RED + "That command does not exist! Use: /rm help", true);
		addMsg("notOnline", ChatColor.RED + "That player is not online!", true);
		addMsg("brNotExists", ChatColor.RED + "That bonus race does not exist!", true);
		addMsg("raceChanged", "%PLAYER% changed your race to %RACE%", true);
		addMsg("raceChanged", "%PLAYER% changed your titan type to %TITAN%", true);
		addMsg("ttNotExist", ChatColor.RED + "That titan type does not exist!", true);
		addMsg("playerNotTitan", ChatColor.RED + "That player is not a titan!", true);
		addMsg("nowHidden", "You are now hidden from the eyes of the world!", false);
		addMsg("unHidden", "You are now visible to the eyes of the world!", false);
		addMsg("nvOn", "You feel the demonic powers invert your light receptors!", false);
		addMsg("nvOff", "You feel the demonic powers within your eyes begin to fade...", false);
		addMsg("noRace", ChatColor.RED + "You can't do this, as you are no %RACE%.", false);
		addMsg("pfOn", "You feel the mighty power of the fire!", false);
		addMsg("pfOff", "The power of the fire flows away...", false);
		addMsg("flyOn", "You feel the holy powers rushing through you...", false);
		addMsg("flyOff", "You feel the holy powers flow away...", false);
		addMsg("healSelf", "You feel a warm glow in your veins, and feel reborn!", false);
		addMsg("waitHeal", ChatColor.RED + "You have to wait %TIME% more seconds before you can heal again!", false);
		addMsg("healOther", "You feel a warm glow in your veins, and feel reborn, as you are healed by %PLAYER%.", false);
		addMsg("psOn", "You feel the demonic powers turn the lava in your veins back into blood!", false);
		addMsg("psOn", "You feel the demonic powers turn the blood in your veins into lava!", false);
		addMsg("alreadyChoseRace", ChatColor.RED + "You have already chosen a race! If you still want a new one, you can buy it here: http://store.aeonblox.com", true);
		addMsg("cantShootArrows", ChatColor.RED + "You can't shoot arrows this fast!", false);
		addMsg("noHigher", ChatColor.RED + "You can't fly any higher than 16 blocks!", false);
	}
	private void addMsg(String name, String message, boolean prefix){
		if (prefix){
			messages.put(name, prefix + message);
		} else {
			messages.put(name, ChatColor.GOLD + message);
		}
	}
	public String getMessage(String message){
		return messages.get(message);
	}
}
