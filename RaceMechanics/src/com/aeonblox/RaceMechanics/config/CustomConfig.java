package com.aeonblox.RaceMechanics.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.aeonblox.RaceMechanics.RMMain;

public class CustomConfig{
	RMMain plugin;
	public CustomConfig(RMMain instance){
		plugin = instance;
	}
	public FileConfiguration getCustomConfig(Config config) {
		if (config.fileConfig == null){
			reloadCustomConfig(config);
		}
		return config.fileConfig;
	}
	public void reloadCustomConfig(Config config) {
		if (config.fileConfig == null) {
			config.file = new File(plugin.getDataFolder(), config.name + ".yml");
		}
		config.fileConfig = YamlConfiguration.loadConfiguration(config.file);

		InputStream defConfigStream = plugin.getResource(config.name + ".yml");
		if (defConfigStream != null) {
			@SuppressWarnings("deprecation")
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			config.fileConfig.setDefaults(defConfig);
		}
	}
	public void saveCustomConfig(Config config) {
		if (config.fileConfig == null || config.file == null) {
			return;
		}
		try {
			getCustomConfig(config).save(config.file);
		} catch (IOException ex) {
			plugin.getLogger().log(Level.SEVERE, "Could not save config to " + config.file, ex);
		}
	}
	public void saveDefaultConfig(Config config) {
		if (config.file == null) {
			config.file = new File(plugin.getDataFolder(), config.name + ".yml");
		}
		if (!config.file.exists()) {
			plugin.saveResource(config.name + ".yml", false);
		}
	}
}