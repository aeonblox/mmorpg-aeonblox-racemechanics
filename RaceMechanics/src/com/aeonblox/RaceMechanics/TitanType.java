package com.aeonblox.RaceMechanics;

import java.util.HashMap;

import org.bukkit.ChatColor;

public enum TitanType {
	Weather("weather"),
	Earth("earth"),
	Water("water"),
	Fire("fire"),
	None("none"),;
	String name;
	public String getName(){
		return this.name;
	}
	TitanType(String name){
		this.name = name;
	}
	static HashMap<String, TitanType> array = new HashMap<String, TitanType>();
	public static TitanType forName(String s){
		if (s == null){
			return TitanType.None;
		}
		TitanType t = array.get(s.toLowerCase());
		if (t == null){
			return TitanType.None;
		} else {
			return t;
		}
	}
	public String getSuffix(){
		if (this.getName() == "weather"){
			return ChatColor.GRAY + "[T]";
		} else if (this.getName() == "water"){
			return ChatColor.AQUA + "[T]";
		} else if (this.getName() == "earth"){
			return ChatColor.DARK_GREEN + "[T]";
		} else {
			return ChatColor.RED + "[T]";
		}
	}
}
