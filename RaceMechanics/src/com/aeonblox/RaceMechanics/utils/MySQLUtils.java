package com.aeonblox.RaceMechanics.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.TitanType;
import com.aeonblox.RaceMechanics.MySQL.MySQL;

public class MySQLUtils {
	RMMain plugin;
	public MySQLUtils(RMMain instance){
		this.plugin = instance;
	}
	protected void reloadMySQL(){
		final Logger log = plugin.getLogger();
		final long currentTime = System.currentTimeMillis();
		plugin.utils.saveRacePlayers();
		plugin.username = plugin.configHandler.getCustomConfig(plugin.config).getString("mysql-username");
		plugin.password = plugin.configHandler.getCustomConfig(plugin.config).getString("mysql-password");
		plugin.ip = plugin.configHandler.getCustomConfig(plugin.config).getString("mysql-host");
		plugin.database = plugin.configHandler.getCustomConfig(plugin.config).getString("mysql-database");
		plugin.port =  plugin.configHandler.getCustomConfig(plugin.config).getString("mysql-port");
		log.info("Attempting to connect to MySQL server with the following details: port: " + plugin.port + ", ip: " + plugin.ip + ", username: " + plugin.username + ", password: " + plugin.password);
		plugin.mySQL = new MySQL(plugin, plugin.ip, plugin.port,plugin.database, plugin.username, plugin.password);
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable(){
			public void run(){
				Bukkit.broadcastMessage(ChatColor.DARK_AQUA + "Starting the reload of all MySQL variables and all RacePlayers...");
				plugin.con = plugin.mySQL.openConnection();
				log.info("Attempting to create database " + plugin.database + " if it doesn't exist....");
				plugin.mySQL.updateSQL("CREATE DATABASE IF NOT EXISTS " + plugin.database);
				log.info("Attempting to create table racePlayers if it doesn't exist....");
				plugin.mySQL.updateSQL("CREATE TABLE IF NOT EXISTS racePlayers(uuid TEXT, BonusRaceType TEXT, RaceType TEXT, TitanType TEXT)");
				plugin.RacePlayers.clear();
				getRacePlayersFromMySQL();
				long currentTime2 = System.currentTimeMillis();
				long s = currentTime2 - currentTime;
				log.info("Finished MySQL setup in " + s + " miliseconds, or " + s/50 + " ticks");
				try {
					plugin.con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				plugin.mySQL.closeConnection();
				Bukkit.broadcastMessage(ChatColor.DARK_AQUA + "finished reloading MySQL and RacePlayers. It took approximately " + s + " milliseconds, or " + s/20 + " server ticks");
			}
		});
	}
	protected RaceType getRaceTypeFromMySQL(UUID player){
		Connection con = plugin.mySQL.openConnection();
		RaceType type;
		try {
			Statement stmt = con.createStatement();
			ResultSet set = stmt.executeQuery("SELECT * FROM racePlayers WHERE uuid=\'" + player.toString() +  "\'");
			type = RaceType.Default;
			if (set.next()){
				type = RaceType.forName(set.getString("RaceType"));
			}
			stmt.close();
			con.close();
			plugin.mySQL.closeConnection();
			return type;
		} catch (SQLException e) {
			e.printStackTrace();
			return RaceType.Default;
		}
	}
	protected TitanType getTitanTypeFromMySQL(UUID player){
		Connection con = plugin.mySQL.openConnection();
		TitanType type = TitanType.None;
		try {
			Statement stmt = con.createStatement();
			ResultSet set = stmt.executeQuery("SELECT * FROM racePlayers WHERE uuid=\'" + player.toString() +  "\'");
			type = TitanType.None;
			if (set.next()){
				type = TitanType.forName(set.getString("TitanType"));
			}
			stmt.close();
			con.close();
			plugin.mySQL.closeConnection();
			return type;
		} catch (SQLException e) {
			e.printStackTrace();
			return TitanType.None;
		}
	}
	protected BonusRaceType getBonusRaceFromMySQL(UUID player){
		Connection con = plugin.mySQL.openConnection();
		try {
			Statement stmt = con.createStatement();
			ResultSet set = stmt.executeQuery("SELECT * FROM racePlayers WHERE uuid=\'" + player.toString() +  "\'");
			BonusRaceType type = BonusRaceType.None;
			if (set.next()){
				type = RaceType.bonusForName(set.getString("BonusRaceType"));
			}
			stmt.close();
			con.close();
			plugin.mySQL.closeConnection();
			return type;
		} catch (SQLException e) {
			e.printStackTrace();
			return BonusRaceType.None;
		}
	}
	protected void getRacePlayersFromMySQL(){

		Connection con = plugin.mySQL.openConnection();
		try{
			final Statement stmt = con.createStatement();
			for (final Player p: Bukkit.getOnlinePlayers()){
				Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable(){
					public void run(){
						ResultSet set;
						try {
							set = stmt.executeQuery("SELECT * FROM racePlayers WHERE uuid=\'" + p.getUniqueId().toString() +  "\'");
							BonusRaceType bonustype = BonusRaceType.None;
							RaceType type = RaceType.Default;
							TitanType titantype = TitanType.None;
							if (set.next()){
								bonustype = RaceType.bonusForName(set.getString("BonusRaceType"));
								type = RaceType.forName(set.getString("RaceType"));
								titantype = TitanType.forName(set.getString("TitanType"));
							}
							set.close();
							new RacePlayer(p.getUniqueId(), type, bonustype, titantype, plugin.utils, plugin);	
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				});
			}
			stmt.close();
			con.close();
			plugin.mySQL.closeConnection();
		} catch (SQLException e){
		}
	}
	protected void savePlayers(){
		for (final Player player : Bukkit.getOnlinePlayers()){
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable(){
				public void run(){
					Connection con = plugin.mySQL.openConnection();
					Statement stmt;
					try {
						stmt = con.createStatement();
						ResultSet set = stmt.executeQuery("SELECT * FROM racePlayers WHERE uuid=\'" + player.getUniqueId().toString() +  "\'");
						RacePlayer plr = plugin.utils.getRacePlayer(player.getUniqueId());
						if (set.next()){
							plugin.mySQL.updateSQL("UPDATE racePlayers SET TitanType=\"" + plr.getTitanType().toString() + "\", BonusRaceType=\"" + plr.getBonusRace().getName() + "\", RaceType=\"" + plr.getRace().getName() + "\" WHERE uuid=\"" + player.getUniqueId().toString() + "\"");
						} else {
							plugin.mySQL.updateSQL("INSERT INTO racePlayers VALUES(\'" + player.getUniqueId().toString() + "\', \'" + plr.getBonusRace().getName() + "\', \'" + plr.getRace().getName() + "\', \'" +  plr.getTitanType().toString() + "\')");
						}	
					} catch (SQLException e) {
						e.printStackTrace();
					}

				}
			});
		}
	}
	protected void savePlayersWithLagg(){
		for (Player player : Bukkit.getOnlinePlayers()){
			Connection con = plugin.mySQL.openConnection();
			Statement stmt;
			try {
				stmt = con.createStatement();
				ResultSet set = stmt.executeQuery("SELECT * FROM racePlayers WHERE uuid=\'" + player.getUniqueId().toString() +  "\'");
				RacePlayer plr = plugin.utils.getRacePlayer(player.getUniqueId());
				if (set.next()){
					plugin.mySQL.updateSQL("UPDATE racePlayers SET TitanType=\"" + plr.getTitanType().toString() + "\", BonusRaceType=\"" + plr.getBonusRace().getName() + "\", RaceType=\"" + plr.getRace().getName() + "\" WHERE uuid=\"" + player.getUniqueId().toString() + "\"");
				} else {
					plugin.mySQL.updateSQL("INSERT INTO racePlayers VALUES(\'" + player.getUniqueId().toString() + "\', \'" + plr.getBonusRace().getName() + "\', \'" + plr.getRace().getName() + "\', \'" +  plr.getTitanType().toString() + "\')");
				}	
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}
	protected void savePlayer(final RacePlayer plr){
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable(){
			public void run(){
				Connection con = plugin.mySQL.openConnection();
				Statement stmt;
				try {
					stmt = con.createStatement();
					ResultSet set = stmt.executeQuery("SELECT * FROM racePlayers WHERE uuid=\'" + plr.getUUID().toString() +  "\'");
					if (set.next()){
						plugin.mySQL.updateSQL("UPDATE racePlayers SET TitanType=\"" + plr.getTitanType().toString() + "\", BonusRaceType=\"" + plr.getBonusRace().getName() + "\", RaceType=\"" + plr.getRace().getName() + "\" WHERE uuid=\"" + plr.getUUID() + "\"");
					} else {
						plugin.mySQL.updateSQL("INSERT INTO racePlayers VALUES(\'" + plr.getUUID().toString() + "\', \'" + plr.getBonusRace().getName() + "\', \'" + plr.getRace().getName() + "\', \'" +  plr.getTitanType().toString() + "\')");
					}	
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		});
	}
	protected void savePlayer(final RacePlayer plr, boolean bool){
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable(){
			public void run(){
				Connection con = plugin.mySQL.openConnection();
				Statement stmt;
				try {
					stmt = con.createStatement();
					ResultSet set = stmt.executeQuery("SELECT * FROM racePlayers WHERE uuid=\'" + plr.getUUID().toString() +  "\'");
					if (set.next()){
						plugin.mySQL.updateSQL("UPDATE racePlayers SET TitanType=\"" + plr.getTitanType().toString() + "\", BonusRaceType=\"" + plr.getBonusRace().getName() + "\", RaceType=\"" + plr.getRace().getName() + "\" WHERE uuid=\"" + plr.getUUID() + "\"");
					} else {
						plugin.mySQL.updateSQL("INSERT INTO racePlayers VALUES(\'" + plr.getUUID().toString() + "\', \'" + plr.getBonusRace().getName() + "\', \'" + plr.getRace().getName() + "\', \'" +  plr.getTitanType().toString() + "\')");
					}	
				} catch (SQLException e) {
					e.printStackTrace();
				}
				plugin.RacePlayers.remove(plr);
			}
		});
	}
}
