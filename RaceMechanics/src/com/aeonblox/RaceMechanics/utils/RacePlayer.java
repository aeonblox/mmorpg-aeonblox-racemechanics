package com.aeonblox.RaceMechanics.utils;

import java.io.PrintStream;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.TitanType;

public class RacePlayer {
	private UUID player;
	private RaceType race = RaceType.Default;
	private BonusRaceType bonusRace = BonusRaceType.None;
	private TitanType titanType = TitanType.None;
	private RMMain plugin;
	//private int money;
	Utils utils;
	//private int plusAmount = 0;
	protected RacePlayer(UUID p, RaceType type, BonusRaceType bonusRace, TitanType titan, Utils utils, RMMain instance){
		this.plugin = instance;
		plugin.RacePlayers.add(this);
		this.player = p;
		this.race = type;
		if (!(titan == TitanType.None) || !(titan == null)){
			this.titanType = titan;
		} else {
			this.titanType = TitanType.None;
		}
		if (!(bonusRace == BonusRaceType.None) || !(bonusRace == null)){
			this.bonusRace = bonusRace;
		}
		this.utils = utils;
		utils.saveRacePlayer(this);
	}
	public Player getPlayer(){
		return Bukkit.getPlayer(player);
	}
	public RaceType getRace(){
		return race;
	}
	public UUID getUUID(){
		return player;
	}
	public void sendMessage(String message){
		Bukkit.getPlayer(player).sendMessage(message);
	}
	public void delete(){
		this.player = null;
		this.race = null;
		this.bonusRace= null;
	}
	public BonusRaceType getBonusRace(){
		if (this.bonusRace == null){
			return BonusRaceType.None;
		} else {
			return this.bonusRace;
		}
	}
	public void setTitanType(TitanType t){
		this.titanType = t;
	}
	public void setBonusRaceType(BonusRaceType type){
		this.bonusRace = type;
	}
	public void setRaceType(RaceType type){
		this.race = type;
		if (type != RaceType.Titan){
			this.setTitanType(TitanType.None);
		}
		Bukkit.getPlayer(player).sendMessage(ChatColor.GOLD + "You are now a " + this.getRace());
		Bukkit.getPlayer(player).setDisplayName(this.getRace().getNameColor() + "" + Bukkit.getPlayer(this.player).getName() + ChatColor.RESET);
	}
	public TitanType getTitanType(){
		if (this.titanType != null && this.titanType != TitanType.None){
			return this.titanType;
		} else {
			return TitanType.None;
		}
	}
	public boolean getErrors(){
		PrintStream out = System.out;
		if (this.player == null){
			System.out.println("Player is null!");
			return true;
		}
		if (this.race == null){
			out.println("Race is null!");
			return true;
		}
		if (this.bonusRace == null){
			out.println("Bonusrace is null!");
			return true;
		}
		if (this.plugin == null){
			out.println("plugin is null!");
			return true;
		}
		return false;
	}
	/*public int setPlusAmount(){
		return plusAmount;
	}
	public void setAmount(int in){
		this.plusAmount = in;
	}*/
}
