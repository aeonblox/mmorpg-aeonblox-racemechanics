package com.aeonblox.RaceMechanics.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.TitanType;

public class Utils {

	RMMain plugin;
	MySQLUtils mySqlUtils;
	private Scoreboard board;
	private Team hiddenTeam;
		
	public Utils(RMMain instance, MySQLUtils mutils){
		plugin = instance;
		this.mySqlUtils = mutils;
		board = plugin.getServer().getScoreboardManager().getMainScoreboard();
		if (board.getTeam("hiddenTeam") == null){
		hiddenTeam = board.registerNewTeam("hiddenTeam");
		} else {
			hiddenTeam = board.getTeam("hiddenTeam");
		}
		hiddenTeam.setNameTagVisibility(NameTagVisibility.NEVER);
	}
	public RMMain getPlugin(){
		return plugin;
	}
	public Team getHiddenTeam(){
		return hiddenTeam;
	}
	public String crs(String command, String explanation, String shortcut){
		return ChatColor.GOLD + "/RaceMechanics "+ ChatColor.YELLOW + command + " " + "\n"  + ChatColor.DARK_AQUA + explanation + ChatColor.GREEN + " shortcuts: " + shortcut;
	}
	public ArrayList<RacePlayer> getRacePlayerList(){
		return plugin.RacePlayers;
	}
	public void sendHelp(Player p){
		p.sendMessage(ChatColor.DARK_AQUA + "RaceMechanics help menu!");
		p.sendMessage(crs("setrace <playername> <racename>", "sets a players race", "sr"));
		p.sendMessage(crs("reload","reloads the RaceMechanics config", "none"));
		p.sendMessage(crs("listplayers", "lists all of the online raceplayers", "none"));
		p.sendMessage(crs("showinfo", "shows your RacePlayer info", "sh"));
		p.sendMessage(crs("giveracepane <race name>", "gives you the race pane of the specified race", "grp"));
		p.sendMessage(crs("help", "displays this message", "none"));
		p.sendMessage(crs("setbonusrace <playername> <race name>", "sets the bonus race of the specified player", "sbr"));
		p.sendMessage(crs("settitantype <playername> <titan type name>", "sets the titan type of the specified player", "stt"));
	}
	/*public void updateRacePlayerList(ArrayList<RacePlayer> list){
		list.clear();
		for (Player p: Bukkit.getOnlinePlayers()){
			UUID uuid = p.getUniqueId();
			list.add(new RacePlayer(uuid, this.getPlayerRaceFromConfig(uuid), this.getBonusRaceFromConfig(uuid), this.getTitanTypeFromConfig(uuid), this, plugin));
		}
	}*/
	public void sendHelp(CommandSender sender){
		sendHelp(((Player)sender));
	}
	public void sendUnknownCommand(CommandSender sender){
		sender.sendMessage(plugin.getMessage("unknownCommand"));
	}
	public void reloadConfig(){
		if (plugin.RacePlayerConfig.file == null || plugin.RacePlayerConfig.fileConfig == null){
			plugin.configHandler.getCustomConfig(plugin.RacePlayerConfig).options().copyDefaults(true);
			plugin.configHandler.saveDefaultConfig(plugin.RacePlayerConfig);
			plugin.configHandler.saveCustomConfig(plugin.RacePlayerConfig);
		} else {
			plugin.configHandler.saveCustomConfig(plugin.RacePlayerConfig);
		}
		if (plugin.config.file == null || plugin.config.fileConfig == null){
			plugin.configHandler.getCustomConfig(plugin.config).options().copyDefaults(true);
			plugin.configHandler.saveDefaultConfig(plugin.config);
			plugin.configHandler.saveCustomConfig(plugin.config);
		} else {
			plugin.configHandler.saveCustomConfig(plugin.config);
		}
		if (plugin.mysqlenabled){
			this.mySqlUtils.reloadMySQL();
		}
	}
	public RacePlayer getRacePlayer(UUID name){
		for (RacePlayer p: plugin.RacePlayers){
			if (p.getUUID() == name){
				return p;
			}
		}
		return null;
	}
	public RacePlayer getRacePlayer(String name){
		for (int i = 0; i < plugin.RacePlayers.size(); i++){
			RacePlayer p = plugin.RacePlayers.get(i);
			if (Bukkit.getPlayer(p.getUUID()).getName().equals(name)){
				return p;
			}
		}
		return null;
	}
	protected RaceType getPlayerRaceFromConfig(UUID player){
		RaceType type = RaceType.Default;
		if (!plugin.mysqlenabled){
			type = RaceType.forName(String.valueOf(plugin.configHandler.getCustomConfig(plugin.RacePlayerConfig).getString("players." + player.toString() + ".normaltype")));
			if (type != null){
				return type;
			} else {
				return RaceType.Default;
			}
		} else {
			return this.mySqlUtils.getRaceTypeFromMySQL(player);
		}
	}
	public RaceType getPlayerRace(UUID player){
		try {
			RaceType type = getRacePlayer(player).getRace();
			return type;
		} catch (Exception ex){
			return RaceType.Default;
		}
	}
	public void getRacePlayers(){
		if (!plugin.mysqlenabled){
			for (Player p: Bukkit.getOnlinePlayers()){
				UUID uuid = p.getUniqueId();  
				new RacePlayer(uuid, this.getPlayerRaceFromConfig(uuid), this.getBonusRaceFromConfig(uuid), this.getTitanTypeFromConfig(uuid), this, plugin);
			}
		} else {
			this.mySqlUtils.getRacePlayersFromMySQL();
		}
	}
	public TitanType getTitanType(UUID player){
		return getRacePlayer(player).getTitanType();
	}
	protected TitanType getTitanTypeFromConfig(UUID player){
		if (!plugin.mysqlenabled){
			return TitanType.forName(plugin.configHandler.getCustomConfig(plugin.RacePlayerConfig).getString("players." +  player.toString() + ".titantype"));
		} else {
			return this.mySqlUtils.getTitanTypeFromMySQL(player);
		}
	}
	public void saveRacePlayers(Boolean boiol){
		if (!plugin.mysqlenabled){
			this.saveRacePlayers();
		} else {
			plugin.mutils.savePlayersWithLagg();
		}
	}
	public void saveRacePlayers(){
		for (Player p:Bukkit.getOnlinePlayers()){
			if (!plugin.mysqlenabled){
				FileConfiguration config = plugin.configHandler.getCustomConfig(plugin.RacePlayerConfig);
				plugin.configHandler.getCustomConfig(plugin.RacePlayerConfig).set("players." + p.getUniqueId().toString() + ".normaltype",  getPlayerRace(p.getUniqueId()).getName());
				if (getBonusRace(p.getUniqueId()) != BonusRaceType.None){
					config.set("players." + p.getUniqueId().toString() + ".bonustype", getBonusRace(p.getUniqueId()).getName());
				} else {
					config.set("players." + p.getUniqueId().toString() + ".bonustype", BonusRaceType.None.getName());
				}
				if (getTitanType(p.getUniqueId()) != TitanType.None){
					config.set("players." + p.getUniqueId().toString() + ".titantype", getTitanType(p.getUniqueId()).getName());
				} else {
					config.set("players." + p.getUniqueId().toString() + ".titantype", TitanType.None.getName());
				}
			} else {
				plugin.mutils.savePlayers();
			}
		}
		plugin.configHandler.getCustomConfig(plugin.config).options().copyDefaults(true);
		plugin.configHandler.saveDefaultConfig(plugin.config);
		plugin.configHandler.saveCustomConfig(plugin.RacePlayerConfig);
	}
	public boolean hasBonusRace(UUID player){
		try {
			if (getRacePlayer(player).getBonusRace() != null && BonusRaceType.forName(plugin.RacePlayerConfig.fileConfig.getString("players." + player.toString() + ".bonustype")) != BonusRaceType.None){
				return true;
			}
			return false;
		} catch (Exception ex){
			return false;
		}
	}
	protected BonusRaceType getBonusRaceFromConfig(UUID player){
		if (!plugin.mysqlenabled){
			return RaceType.bonusForName(plugin.configHandler.getCustomConfig(plugin.RacePlayerConfig).getString("players." + player.toString() + ".bonustype"));
		} else {
			return this.mySqlUtils.getBonusRaceFromMySQL(player);
		}
	}
	public BonusRaceType getBonusRace(UUID player){
		return getRacePlayer(player).getBonusRace();
	}
	public int randInt(int min, int max) {
		Random rand = new Random();   
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}
	public void changeNameLater(final UUID player){
		try{
			Bukkit.getPlayer(player).setDisplayName(getPlayerRace(player).getNameColor() + "" + Bukkit.getPlayer(player).getName() + ""+ ChatColor.RESET + " "+ getBonusRace(player).getSuffix());
		} catch (Exception ex){
		}
	}
	public String formatToRaceName(UUID player){
		if (getTitanType(player) != TitanType.None && getBonusRace(player) == BonusRaceType.None){
			return   getPlayerRace(player).getNameColor() + Bukkit.getPlayer(player).getName() + getTitanType(player).getSuffix();
		}
		else if (getTitanType(player) != TitanType.None && getBonusRace(player) != BonusRaceType.None){
			return getPlayerRace(player).getNameColor() + Bukkit.getPlayer(player).getName() + getTitanType(player).getSuffix() + getBonusRace(player).getSuffix();
		}
		else if (getBonusRace(player) == BonusRaceType.None){
			return getRacePlayer(player).getRace().getNameColor() + Bukkit.getPlayer(player).getName(); 
		} else {
			return getRacePlayer(player).getRace().getNameColor() + Bukkit.getPlayer(player).getName() + getBonusRace(player).getSuffix();
		}
	}
	public void saveRacePlayer(RacePlayer plr){
		if (plugin.mysqlenabled){
			plugin.mutils.savePlayer(plr);
		} else {
			plugin.configHandler.reloadCustomConfig(plugin.config);
			plugin.configHandler.getCustomConfig(plugin.config).set("players." + plr.getUUID().toString() + ".normaltype", plr.getRace().getName());
			plugin.configHandler.getCustomConfig(plugin.config).set("players." + plr.getUUID().toString() + ".titantype", plr.getTitanType().getName());
			plugin.configHandler.getCustomConfig(plugin.config).set("players." + plr.getUUID().toString() + ".bonustype", plr.getBonusRace().getName());
			plugin.configHandler.saveCustomConfig(plugin.config);	
		}
	}
	public void saveRacePlayer(RacePlayer player, boolean bool){
		if (plugin.mysqlenabled){
			plugin.mutils.savePlayer(player, true);
		} else {
			plugin.configHandler.reloadCustomConfig(plugin.config);
			plugin.configHandler.getCustomConfig(plugin.config).set("players." + player.getUUID().toString() + ".normaltype", player.getRace().getName());
			plugin.configHandler.getCustomConfig(plugin.config).set("players." + player.getUUID().toString() + ".titantype", player.getTitanType().getName());
			plugin.configHandler.getCustomConfig(plugin.config).set("players." + player.getUUID().toString() + ".bonustype", player.getBonusRace().getName());
			plugin.configHandler.saveCustomConfig(plugin.config);
			plugin.RacePlayers.remove(player);
		}
	}
	public void createPlayer(final UUID u){
		final Utils ut = this;
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			public void run() {
				RacePlayer pl = new RacePlayer(u, getPlayerRaceFromConfig(u), getBonusRaceFromConfig(u), getTitanTypeFromConfig(u), ut, plugin);
				boolean bool = pl.getErrors();
				if (bool) {
					System.out.println("The raceplayers aren't registering!!!!!!");
				}
				final RaceType type = pl.getRace();
				final BonusRaceType bonusType = pl.getBonusRace();
				Bukkit.getScheduler().runTask(plugin, new Runnable() {
					public void run() {
						/*
						 * Normal Races
						 *
						 */
						if (type == RaceType.Demon) {
							Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1));
						} else if (type == RaceType.Wraith) {
							for (Entity e : Bukkit.getPlayer(u).getNearbyEntities(15, 15, 15)) {
								if (e instanceof Monster) {
									if (!Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
										Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 240, 0), true);
									}
								}
							}
							if(day()) {
								if (!Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.WEAKNESS)) {
									Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0), true);
								}
								if (Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
									Collection<PotionEffect> pe = Bukkit.getPlayer(u).getActivePotionEffects();
									for (PotionEffect effect : pe) {
										if (effect.getType().equals(PotionEffectType.INCREASE_DAMAGE)) {
											if (effect.getAmplifier() > 0) {
												Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
											}
										}
									}
								}
							}else{
								if (!Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
									Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1), true);
								}

								if (Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.WEAKNESS)) {
									Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.WEAKNESS);
								}
								if (Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
									Collection<PotionEffect> pe = Bukkit.getPlayer(u).getActivePotionEffects();
									for (PotionEffect effect : pe) {
										if (effect.getType().equals(PotionEffectType.INCREASE_DAMAGE)) {
											if (effect.getAmplifier() == 0) {
												Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
												Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1), true);

											}
										}
									}
								}
							}

						} else if (type == RaceType.Nymph) {


						} else if (type == RaceType.Priest) {
							if (isWearingGold(Bukkit.getPlayer(u))) {
								if (!Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
									Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1), true);
								}
								if (Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.WEAKNESS)) {
									Collection<PotionEffect> pe = Bukkit.getPlayer(u).getActivePotionEffects();
									for (PotionEffect effect : pe) {
										if (effect.getType().equals(PotionEffectType.WEAKNESS)) {
											if (effect.getAmplifier() == 0) {
												Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.WEAKNESS);
												Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 1), true);

											}
										}
									}
								}

								if (!Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)) {
									Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1), true);
								}

							}

							if (!isWearingGold(Bukkit.getPlayer(u))) {
								if (Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.WEAKNESS)) {
									Collection<PotionEffect> pe = Bukkit.getPlayer(u).getActivePotionEffects();
									for (PotionEffect effect : pe) {
										if (effect.getType().equals(PotionEffectType.WEAKNESS)) {
											if (effect.getAmplifier() == 0) {
												Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.WEAKNESS);
												Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0), true);

											}
										}
									}
								}
								if (Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
									Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
								}
								if (!Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.WEAKNESS)) {
									Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0), true);
								}
								if (!Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)) {
									Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1), true);
								}

							}
							//Bukkit.getPlayer(u).setMaxHealth(20 * 1.3);

						} else if (type == RaceType.Elf) {
							Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2));
							Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
							//Bukkit.getPlayer(u).setMaxHealth(20 * 0.60);
						} else {
							//Bukkit.getPlayer(u).setMaxHealth(20);
						}

						/*
						 * Bonus Races
						 *
						 */
						if (bonusType == BonusRaceType.Paladin) {
							Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, Integer.MAX_VALUE, 1));
						} else if (bonusType == BonusRaceType.Angel) {
							Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2));
							if (Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.JUMP)) {
								Collection<PotionEffect> pe = Bukkit.getPlayer(u).getActivePotionEffects();
								for (PotionEffect effect : pe) {
									if (effect.getType().equals(PotionEffectType.JUMP)) {
										if (2 > effect.getAmplifier()) {
											Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.JUMP);
											Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2), true);

										}
									}
								}
							}
						} else if (bonusType == BonusRaceType.Assassin) {
							Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 3));
							if (Bukkit.getPlayer(u).hasPotionEffect(PotionEffectType.JUMP)) {
								Collection<PotionEffect> pe = Bukkit.getPlayer(u).getActivePotionEffects();
								for (PotionEffect effect : pe) {
									if (effect.getType().equals(PotionEffectType.JUMP)) {
										if (3 > effect.getAmplifier()) {
											Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.JUMP);
											Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 3), true);

										}
									}
								}
							}
							//Bukkit.getPlayer(u).addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
						}
					}
				});

			}
		});
	}

	private boolean day() {
		Server server = Bukkit.getServer();
		long time = server.getWorld("Aeonblox").getTime();

		return time < 12300 || time > 23850;
	}

	private boolean isWearingGold(Player player){
		PlayerInventory inv = player.getInventory();
		ItemStack helmet = inv.getHelmet();
		ItemStack chest = inv.getChestplate();
		ItemStack legs = inv.getLeggings();
		ItemStack boots = inv.getLeggings();
		if (helmet != null){
			if (helmet.getType() == Material.GOLD_HELMET){
				return true;
			}
		}
		if (chest != null){
			if (chest.getType() == Material.GOLD_CHESTPLATE){
				return true;
			}
		}
		if (legs != null){
			if (legs.getType() == Material.GOLD_LEGGINGS){
				return true;
			}
		}
		if (boots != null){
			if (boots.getType() == Material.GOLD_BOOTS){
				return true;
			}
		}
		return false;
	}

}