package com.aeonblox.RaceMechanics.events.player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;
import com.aeonblox.rpg.managers.PlayerManager;

public class PlayerMoveEventListener
implements Listener
{
	Map<String, ArrayList<Block>> vineMap = new HashMap<String, ArrayList<Block>>();
	public RMMain plugin;
	public Utils utils;

	public PlayerMoveEventListener(RMMain instance, Utils utils)
	{
		this.plugin = instance;
		this.utils = utils;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		Player player = event.getPlayer();
		boolean isElf = utils.getPlayerRace(player.getUniqueId()) == RaceType.Elf;
		boolean isChameleon = utils.getPlayerRace(event.getPlayer().getUniqueId()) == RaceType.Chameleon;
		boolean isAssassin = utils.hasBonusRace(event.getPlayer().getUniqueId()) && utils.getBonusRace(player.getUniqueId()) == BonusRaceType.Assassin;
		if (isElf || isChameleon || isAssassin)
		{
			BlockFace bf = yawToFace(player.getLocation().getYaw());
			Block block = player.getLocation().getBlock().getRelative(bf);
			if (block.getType() != Material.AIR)
			{
				for (int i = 0; i < 300; i++)
				{
					Block temp = block.getLocation().add(0.0D, i, 0.0D).getBlock();
					Block opp = player.getLocation().add(0.0D, i, 0.0D).getBlock();
					Block aboveOpp = opp.getLocation().add(0.0D, 1.0D, 0.0D).getBlock();
					int counter = 0;
					for (int k = 0; k < plugin.noVineBlocks.size(); k++) {
						if ((temp.getType() != Material.AIR) && (temp.getType() != plugin.noVineBlocks.get(k))) {
							counter++;
						}
					}
					if ((counter != plugin.noVineBlocks.size()) || ((opp.getType() != Material.AIR) && (opp.getType() != Material.LONG_GRASS) && (opp.getType() != Material.YELLOW_FLOWER) && (opp.getType() != Material.RED_ROSE))) {
						break;
					}
					if (aboveOpp.getType() == Material.AIR)
					{
						player.sendBlockChange(opp.getLocation(), Material.VINE, (byte)0);
						addVines(player, opp);
					}
					player.setFallDistance(0.0F);
				}
			}
			else
			{
				for (int i = 0; i < getVines(player).size(); i++) {
					player.sendBlockChange(((Block)getVines(player).get(i)).getLocation(), Material.AIR, (byte)0);
				}
				getVines(player).clear();
			}
		} else if (utils.getPlayerRace(player.getUniqueId()) == RaceType.Nymph){
			boolean isSpeedSwimming = false;
			boolean isFlying = false;
			//int stopEvent = 1;
			Material m = player.getLocation().getBlock().getType();
			if ((m == Material.STATIONARY_WATER || m == Material.WATER) && isSpeedSwimming == false) {
				isSpeedSwimming = true;
			}

			if (m == Material.AIR || (!(m == Material.STATIONARY_WATER || m == Material.WATER)) ) {
				isSpeedSwimming = false;
			}

			if(player.isFlying() && m == Material.AIR && !(utils.getBonusRace(player.getUniqueId()) == BonusRaceType.Angel)) {
				player.setAllowFlight(false);
				player.setFlying(false);
				isFlying = false;
			}

			if(isSpeedSwimming == false) {
				if(!player.hasPotionEffect(PotionEffectType.SPEED)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0), true);
				}
				if(!player.hasPotionEffect(PotionEffectType.JUMP)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 0), true);
				}
				if(!player.hasPotionEffect(PotionEffectType.WEAKNESS)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0), true);
				}
				if(player.hasPotionEffect(PotionEffectType.WATER_BREATHING)) {
					player.removePotionEffect(PotionEffectType.WATER_BREATHING);
				}
				if(isFlying == true) {
					player.setAllowFlight(false);
					player.setFlying(false);
					isFlying = false;
				}
				//if(stopEvent == 0) {
				//	stopEvent = 1;
				//}

			}

			//if(stopEvent == 0) { return; }

			if(isSpeedSwimming == true) {
				if(!player.hasPotionEffect(PotionEffectType.WATER_BREATHING)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 1), true);
				}
				if(!player.hasPotionEffect(PotionEffectType.CONFUSION)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 40, 1), true);
				}
				if(!player.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 600, 1), true);
				}
				if(!player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 600, 1), true);
				}
				if(!player.hasPotionEffect(PotionEffectType.REGENERATION)) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 120, 1), true);
				}
				if(player.hasPotionEffect(PotionEffectType.WEAKNESS)) {
					player.removePotionEffect(PotionEffectType.WEAKNESS);
				}

				player.setAllowFlight(true);
				player.setFlying(true);
				player.setFlySpeed(0.0425F);
				isFlying = true;
				//stopEvent = 0;
				//if (PlayerManager.getPlayerModel(player).getToggleList() != null && PlayerManager.getPlayerModel(player).getToggleList().contains("debug")) {
				//	player.sendMessage(ChatColor.AQUA + "Speeeed! Speeed! Speed! You feel the aqua rejuvinate you...");
				//}
			}

		}
	}

	public ArrayList<Block> getVines(Player player)
	{
		if (this.vineMap.containsKey(player.getName())) {
			return (ArrayList<Block>)this.vineMap.get(player.getName());
		}
		ArrayList<Block> temp = new ArrayList<Block>();
		return temp;
	}

	public void setVines(Player player, ArrayList<Block> vines)
	{
		this.vineMap.put(player.getName(), vines);
	}

	public void addVines(Player player, Block vine)
	{
		ArrayList<Block> updated = new ArrayList<Block>();
		updated = getVines(player);
		updated.add(vine);
		setVines(player, updated);
	}

	public BlockFace yawToFace(float yaw)
	{
		BlockFace[] axis = { BlockFace.SOUTH, BlockFace.WEST, BlockFace.NORTH, BlockFace.EAST };
		return axis[(java.lang.Math.round(yaw / 90.0F) & 0x3)];
	}
}