package com.aeonblox.RaceMechanics.events.player;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class BowHitEvent implements Listener{
	Utils utils;
	public BowHitEvent(Utils utils){
		this.utils = utils;
	}
	@EventHandler
	public void onReceiveArrowDMG(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Projectile){
			Projectile damager = (Projectile) e.getDamager();
			if (damager.getShooter() instanceof Player){
				RaceType type = utils.getPlayerRace(((Player) damager.getShooter()).getUniqueId());
				BonusRaceType bonusType = utils.getBonusRace(((Player)damager.getShooter()).getUniqueId());
				if (type == RaceType.Wraith){
					if (e.getEntity() instanceof Player){
						((Player) e.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 1, 10));
					}
				} else if (type == RaceType.Demon){
					e.getEntity().setFireTicks(50);
				}
				if (bonusType == BonusRaceType.Assassin){
					((LivingEntity) e.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 1, 10));
				}
			}
		}
	}
}
