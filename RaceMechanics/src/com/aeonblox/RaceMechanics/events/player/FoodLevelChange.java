package com.aeonblox.RaceMechanics.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class FoodLevelChange implements Listener{
	Utils utils;
	public FoodLevelChange(Utils utils){
		this.utils = utils;
	}
	@EventHandler
	public void food(FoodLevelChangeEvent e){
		if (utils.getPlayerRace(e.getEntity().getUniqueId()) == RaceType.Wraith || utils.getPlayerRace(e.getEntity().getUniqueId()) == RaceType.Priest || utils.getBonusRace(e.getEntity().getUniqueId()) == BonusRaceType.Angel){
			int change = ((Player) e.getEntity()).getFoodLevel() - e.getFoodLevel();
			if (change > 0){
				((Player) e.getEntity()).setFoodLevel(((Player) e.getEntity()).getFoodLevel() - (change/2));
			}
		}
	}
}
