package com.aeonblox.RaceMechanics.events.player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class FireDamage implements Listener{
	RMMain plugin;
	Utils utils;
	public FireDamage(RMMain instance, Utils utils){
		this.plugin = instance;
		this.utils = utils;
	}
	@EventHandler (ignoreCancelled = true)
	public void fire(EntityDamageEvent e){
		try {
			if (utils.getPlayerRace(e.getEntity().getUniqueId()) != null && e.getEntity() instanceof Player){
				RaceType type = utils.getPlayerRace(e.getEntity().getUniqueId());
				if (e.getCause() == DamageCause.FIRE || e.getCause() == DamageCause.FIRE_TICK){
					if (utils.hasBonusRace(e.getEntity().getUniqueId()) && utils.getBonusRace(e.getEntity().getUniqueId())== BonusRaceType.Paladin){
						e.setDamage(e.getDamage()* 0.6D);
					}
					if (type == RaceType.Demon){
						e.setCancelled(true);
						e.getEntity().setFireTicks(0);
					}
					if (type == RaceType.Nymph){
						if (!e.getEntity().isDead()){
							try {
								e.setDamage(e.getDamage() * 1.4D);
							} catch (Exception ex){
								((Player)e.getEntity()).setHealth(0.0D);
							}
						}
					}
					if(type == RaceType.Wraith){
						e.setDamage(e.getDamage() * 0.8D);
					}
				}
				if (e.getCause() == DamageCause.LAVA){
					if (type == RaceType.Demon){
						e.setCancelled(true);
						e.setDamage(0);
					}
				}
			}
		} catch (Exception ex){

		}
	}
}
