package com.aeonblox.RaceMechanics.events.player;

import com.aeonblox.rpg.ItemMechanics.ItemMechanics;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;
import com.aeonblox.rpg.DuelMechanics.DuelMechanics;
import com.aeonblox.rpg.HealthMechanics.HealthMechanics;
import com.aeonblox.rpg.LevelMechanics.PlayerLevel;
import com.aeonblox.rpg.managers.PlayerManager;

public class PlayerDamage implements Listener{
    Utils utils;
    public PlayerDamage(Utils utils){
        this.utils = utils;
    }
    @EventHandler (priority = EventPriority.MONITOR)
    public void dmg(EntityDamageByEntityEvent e){
        if (e.getDamager() instanceof Player){
            RaceType type = utils.getPlayerRace(e.getDamager().getUniqueId());
            Player player = (Player) e.getDamager();
            PlayerLevel pLevel = PlayerManager.getPlayerModel(player).getPlayerLevel();
            if (utils.hasBonusRace(e.getDamager().getUniqueId()) && utils.getBonusRace(e.getDamager().getUniqueId()) == BonusRaceType.Paladin){
                e.setDamage(e.getDamage() * 1.15);
                int numb = utils.randInt(0, 100);
                if (numb < 40){
                    e.getDamager().getWorld().strikeLightning(e.getEntity().getLocation());
                    player.sendMessage(ChatColor.GOLD + "Palatial Strike!");
                    e.setDamage(e.getDamage() * 1.05);
                }
                return;
            }
            if (utils.hasBonusRace(e.getDamager().getUniqueId()) && utils.getBonusRace(e.getDamager().getUniqueId()) == BonusRaceType.Assassin){
                Player dmgr = (Player) e.getDamager();
                ItemStack stack = dmgr.getItemInHand();
                Material t = (dmgr.getItemInHand() == null ? Material.AIR : stack.getType());
                if (stack != null && (t == Material.IRON_SPADE || t == Material.WOOD_SPADE || t == Material.GOLD_SPADE || t == Material.DIAMOND_SPADE || t == Material.STONE_SPADE)){
                    LivingEntity victim = (LivingEntity) e.getEntity();
                    Double baseDMG = e.getDamage();

                    double q = victim.getLocation().getDirection().dot(player.getLocation().getDirection());
                    if (q > 0.0D) {
                        int bonus = (int) (q * utils.randInt(0, 8) * baseDMG);
                        if (bonus == 0) {
                            player.sendMessage(ChatColor.DARK_GRAY + "You're backstab failed! You've discovered...");
                            player.sendMessage(ChatColor.DARK_GRAY + "You should probably start running now.");
                            return;
                        }

                        player.sendMessage(ChatColor.DARK_GRAY + "Backstab: " + bonus * e.getDamage() + " extra damage");
                        if ((victim instanceof Player)) {
                            ((Player) e.getEntity()).sendMessage(ChatColor.DARK_GRAY + "You've been stabbed in the back!");
                            ((Player) e.getEntity()).sendMessage(ChatColor.DARK_GRAY + "Turn and react quickly before it's too late!");
                        }
                    }
                }

            }
            if (type == RaceType.Demon){
                if(player.getFireTicks() != 0){
                    e.setDamage(e.getDamage() * 1.25);
                }
                if (player.getItemInHand().getType() == null || player.getItemInHand().getType() == Material.AIR){
                    if(!DuelMechanics.isDamageDisabled((e.getDamager()).getLocation())) {
                        e.getEntity().setFireTicks(15 + (5 * (pLevel.getLevel() / 20)));
                        e.getEntity().getWorld().playEffect(e.getEntity().getLocation().add(0, 1.3, 0), Effect.POTION_BREAK, 8233);
                    }
                }
            }
            else if (type == RaceType.Nymph){
                if (player.getLocation().getBlock().getType() == Material.WATER || player.getLocation().getBlock().getType() == Material.STATIONARY_WATER){
                    e.setDamage(e.getDamage() * 1.6);
                }
                if (player.getItemInHand().getType() == null || player.getItemInHand().getType() == Material.AIR){
                    if(!DuelMechanics.isDamageDisabled((e.getDamager()).getLocation())) {
                        if (!((LivingEntity)e.getEntity()).hasPotionEffect(PotionEffectType.SLOW)) {
                            if (PlayerManager.getPlayerModel((Player) e.getDamager()).getToggleList() != null && PlayerManager.getPlayerModel((Player) e.getDamager()).getToggleList().contains("debug")) {
                                e.getDamager().sendMessage(ChatColor.AQUA + "You just froze...whatever you hit to the core!");
                            }
                            if(((Player) e.getEntity()) instanceof Player) {
                                if (PlayerManager.getPlayerModel(((Player) e.getEntity())).getToggleList() != null && PlayerManager.getPlayerModel(((Player) e.getEntity())).getToggleList().contains("debug")) {
                                    ((Player) e.getEntity()).sendMessage(ChatColor.AQUA + "Pssssh! Freeze!");
                                }
                            }

                            ((LivingEntity)e.getEntity()).getWorld().playEffect(((LivingEntity)e.getEntity()).getLocation().add(0, 0.5, 0), Effect.POTION_BREAK, 8194);
                            ((LivingEntity)e.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 50, 0), true);
                        }
                    }
                }
            }
            else if (type == RaceType.Human){
                //e.setDamage(e.getDamage() * (((Player) e.getEntity()).getLevel()*1.015));
                Double damage = e.getDamage();
                double iTry = 0;
                if(e.getEntity() instanceof Player){
                    PlayerLevel entLevel = PlayerManager.getPlayerModel((Player) e.getEntity()).getPlayerLevel();
                    iTry = (entLevel.getLevel() / 105) * 50;
                }
                Double silentProgression = damage + ((pLevel.getLevel() / 20) * 25) + iTry;
                e.setDamage(silentProgression);
            }
            else if (type == RaceType.Priest){
                e.setDamage(e.getDamage() * 0.85);
                if (player.getMaxHealth() - player.getHealth() > 1){
                    HealthMechanics.setPlayerHP(player.getName(), (int) (player.getHealth() + 1));
                }
            }

            else if (type == RaceType.Wraith) {
                if (player.getItemInHand().getType() == Material.IRON_SWORD){
                    double damage = e.getDamage();
                    e.setDamage(damage * 1.5);
                    //((Player) e.getEntity()).setMaxHealth(((Player) e.getEntity()).getMaxHealth() * 1.25);
                    //player.setHealth(player.getHealth() + damage * 0.6);
                }
                double baseDMG = e.getDamage();
                double leechDMG = (baseDMG / 2);
                if (leechDMG < 1) {
                    leechDMG = 1;
                }

                if ((leechDMG + HealthMechanics.getPlayerHP(player.getName())) > getMaxHP(player)) {
                    HealthMechanics.setPlayerHP(player.getName(), getMaxHP(player));
                    player.setHealth(20);
                } else {
                    if (HealthMechanics.getPlayerHP(player.getName()) > 0 && !player.isDead()) {
                        // They are dead and dont need to be healed -_-
                        HealthMechanics.setPlayerHP(player.getName(), ((int) (HealthMechanics.getPlayerHP(player.getName()) + leechDMG)));
                        double health_percent = ((double) HealthMechanics.getPlayerHP(player.getName())) / (double) getMaxHP(player);
                        double new_health_display = health_percent * 20.0D;
                        player.setHealth((int) new_health_display);
                    }

                    if (PlayerManager.getPlayerModel(player).getToggleList().contains("debug")) {
                        player.sendMessage(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "        +" + ChatColor.DARK_GRAY + leechDMG + ChatColor.BOLD + " HP"
                                + ChatColor.DARK_GRAY + " [" + (int) (HealthMechanics.getPlayerHP(player.getName())) + "/" + (int) getMaxHP(player) + "HP]");
                    }
                }
            } else if (type == RaceType.Titan){
                e.setDamage(e.getDamage() * 0.75);
            } else if (type == RaceType.Elf){
                Player dmgr = (Player) e.getDamager();
                ItemStack stack = dmgr.getItemInHand();
                Material t = (dmgr.getItemInHand() == null ? Material.AIR : stack.getType());
                if (utils.getPlayerRace(dmgr.getUniqueId()) == RaceType.Elf && stack != null && (t == Material.IRON_AXE || t == Material.WOOD_AXE || t == Material.GOLD_AXE || t == Material.DIAMOND_AXE || t == Material.STONE_AXE)){
                    e.setDamage(e.getDamage() * 1.3);
                }
            }
        }

        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            PlayerLevel dmgedLevel = PlayerManager.getPlayerModel(player).getPlayerLevel();
            RaceType type = utils.getPlayerRace(e.getDamager().getUniqueId());

            if(((Player) e.getEntity()).isFlying() && (!player.isOp())) {
                player.setFlying(false);
                player.setAllowFlight(false);
            }
            /*
             *
             * Damage Input / DMG From Entity --> Race Type /
             * Variables : player = Entity
             */
            if (utils.hasBonusRace(e.getEntity().getUniqueId()) && utils.getBonusRace(e.getEntity().getUniqueId()) == BonusRaceType.Paladin){
                e.setDamage(e.getDamage() * 1.25);
            }
            if (utils.getPlayerRace(player.getUniqueId()) == RaceType.Elf){
                if (isWearingGoldOrDiamond(player)) {
                    e.setDamage(e.getDamage() * 1.4D);
                }
            }
            if (utils.getPlayerRace(player.getUniqueId()) == RaceType.Chameleon) {
                e.setDamage(e.getDamage() * 1.15);
            }
            if (utils.getPlayerRace(player.getUniqueId()) == RaceType.Priest){
                if (hasArmor(player, Material.DIAMOND_HELMET, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_LEGGINGS, Material.DIAMOND_BOOTS)) {
                    e.setDamage(e.getDamage() * 1.66);
                }
            }
            if (utils.getPlayerRace(player.getUniqueId()) == RaceType.Wraith){
                if (e.getDamager() instanceof Player){
                    if (player.getItemInHand().getType() == Material.IRON_SWORD){
                        e.setDamage(e.getDamage() * 1.3);
                    }
                }
                if (e.getCause() == DamageCause.POISON){
                        e.setCancelled(true);
                        player.removePotionEffect(PotionEffectType.POISON);
                    }
            }


            /*
             *
             * Damage Output / DMG From Race Type --> Entity /
             * Variables: Type = Damager's Race
             */
            if (utils.hasBonusRace(e.getDamager().getUniqueId()) && utils.getBonusRace(e.getDamager().getUniqueId()) == BonusRaceType.Assassin) {
                if (!(DuelMechanics.isDamageDisabled(player.getLocation()))) {
                    if (((Player)e.getDamager()).getItemInHand().getType() == null || ((Player)e.getDamager()).getItemInHand().getType() == Material.AIR) {
                        int levelBoost = 0;
                        if(e.getDamager() instanceof Player) {
                            PlayerLevel dmgerLevel = PlayerManager.getPlayerModel((Player) e.getDamager()).getPlayerLevel();
                            levelBoost = (5 * (dmgerLevel.getLevel() / 20));
                        }
                        player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, (25 + levelBoost), 0), true);
                        player.getWorld().playEffect(player.getLocation().add(0, 1.3, 0), Effect.POTION_BREAK, 4);
                        e.setDamage(e.getDamage() * 1.1);
                    }
                }
            }
            if (utils.hasBonusRace(e.getDamager().getUniqueId()) && utils.getBonusRace(e.getDamager().getUniqueId()) == BonusRaceType.Angel){
                e.setDamage(e.getDamage() * 0.55);
            }

            if (type == RaceType.Priest){
                e.setDamage(e.getDamage() * 0.85);
            }
            else if (type == RaceType.Titan){
                e.setDamage(e.getDamage() * 0.4);
            } else if (type == RaceType.Elf){
                e.setDamage(e.getDamage() * 1.1);
            } else if (type == RaceType.Chameleon){
                if(!(DuelMechanics.isDamageDisabled(player.getLocation()))) {
                    if (((Player)e.getDamager()).getItemInHand().getType() == null || ((Player)e.getDamager()).getItemInHand().getType() == Material.AIR) {
                        int levelBoost = 0;
                        if(e.getDamager() instanceof Player) {
                            PlayerLevel dmgerLevel = PlayerManager.getPlayerModel((Player) e.getDamager()).getPlayerLevel();
                            levelBoost = (5 * (dmgerLevel.getLevel() / 20));
                        }
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (40 + levelBoost), 1), true);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, (40 + levelBoost), 0), true);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, (40 + levelBoost), 1), true);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, (40 + levelBoost), 0), true);
                        player.getWorld().playEffect(player.getLocation().add(0, 1.3, 0), Effect.POTION_BREAK, 4);
                        e.setDamage(e.getDamage() * 1.1);
                    }
                }
            } else if (type == RaceType.Demon && isWearingIron(player)){
                e.setDamage(e.getDamage() * 1.10);
            } else if (type == RaceType.Nymph && utils.getPlayerRace(player.getUniqueId()) == RaceType.Demon){
                e.setDamage(e.getDamage() * 1.10);
            } else if (type == RaceType.Demon && utils.getPlayerRace(player.getUniqueId()) == RaceType.Nymph){
                e.setDamage(e.getDamage() * 1.05);
            } else if (type == RaceType.Nymph){
                    Double initialDamage = e.getDamage();
                    Double multipliedDamage = initialDamage * 1.4;
                    player.sendMessage("[1] Initial Damage: " + initialDamage);
                    player.sendMessage("[1] Initial Damage: " + initialDamage);
                    e.getDamager().sendMessage("[2] Multiplied Damage: " + multipliedDamage);
                    e.getDamager().sendMessage("[2] Multiplied Damage: " + multipliedDamage);
                    e.setDamage(multipliedDamage);
                if(!DuelMechanics.isDamageDisabled(player.getLocation())) {
                    if (((Player)e.getDamager()).getItemInHand().getType() == null || ((Player)e.getDamager()).getItemInHand().getType() == Material.AIR) {
                        if (!player.hasPotionEffect(PotionEffectType.SLOW)) {
                            if (PlayerManager.getPlayerModel((Player) e.getDamager()).getToggleList() != null && PlayerManager.getPlayerModel((Player) e.getDamager()).getToggleList().contains("debug")) {
                                e.getDamager().sendMessage(ChatColor.AQUA + "You just froze...whatever you hit to the core!");
                            }
                            if (PlayerManager.getPlayerModel(((Player) e.getEntity())).getToggleList() != null && PlayerManager.getPlayerModel(((Player) e.getEntity())).getToggleList().contains("debug")) {
                                player.sendMessage(ChatColor.AQUA + "Pssssh! Freeze!");
                            }
                            int levelBoost = 0;
                            if(e.getDamager() instanceof Player) {
                                PlayerLevel dmgerLevel = PlayerManager.getPlayerModel((Player) e.getDamager()).getPlayerLevel();
                                levelBoost = (5 * (dmgerLevel.getLevel() / 20));
                            }player.getWorld().playEffect(((LivingEntity)e.getEntity()).getLocation().add(0, 0.5, 0), Effect.POTION_BREAK, 8194);
                            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (40 + levelBoost), 0), true);
                        }
                    }
                }
            }

        }
    }

    public int getMaxHP(Player p) {
        if (!HealthMechanics.health_data.containsKey(p.getName())) {
            return 100;
        }
        return HealthMechanics.health_data.get(p.getName());
    }

    public static boolean hasArmor(Player player, Material hat, Material shirt, Material pants, Material feet){
        PlayerInventory inv = player.getInventory();
        ItemStack helmet = inv.getHelmet();
        ItemStack chest = inv.getChestplate();
        ItemStack legs = inv.getLeggings();
        ItemStack boots = inv.getLeggings();
        if (helmet != null){
            if (helmet.getType() == hat){
                return true;
            }
        }
        if (chest != null){
            if (chest.getType() == shirt){
                return true;
            }
        }
        if (legs != null){
            if (legs.getType() == pants){
                return true;
            }
        }
        if (boots != null){
            if (boots.getType() == feet){
                return true;
            }
        }
        return false;
    }

    private boolean isWearingGoldOrDiamond(Player player){
        PlayerInventory inv = player.getInventory();
        ItemStack helmet = inv.getHelmet();
        ItemStack chest = inv.getChestplate();
        ItemStack legs = inv.getLeggings();
        ItemStack boots = inv.getLeggings();
        if (helmet != null){
            if (helmet.getType() == Material.GOLD_HELMET || helmet.getType() == Material.DIAMOND_HELMET){
                return true;
            }
        }
        if (chest != null){
            if (chest.getType() == Material.GOLD_CHESTPLATE || chest.getType() == Material.DIAMOND_CHESTPLATE){
                return true;
            }
        }
        if (legs != null){
            if (legs.getType() == Material.GOLD_LEGGINGS || legs.getType() == Material.DIAMOND_LEGGINGS){
                return true;
            }
        }
        if (boots != null){
            if (boots.getType() == Material.GOLD_BOOTS || boots.getType() == Material.DIAMOND_BOOTS){
                return true;
            }
        }
        return false;
    }
    private boolean isWearingGold(Player player){
        PlayerInventory inv = player.getInventory();
        ItemStack helmet = inv.getHelmet();
        ItemStack chest = inv.getChestplate();
        ItemStack legs = inv.getLeggings();
        ItemStack boots = inv.getLeggings();
        if (helmet != null){
            if (helmet.getType() == Material.GOLD_HELMET){
                return true;
            }
        }
        if (chest != null){
            if (chest.getType() == Material.GOLD_CHESTPLATE){
                return true;
            }
        }
        if (legs != null){
            if (legs.getType() == Material.GOLD_LEGGINGS){
                return true;
            }
        }
        if (boots != null){
            if (boots.getType() == Material.GOLD_BOOTS){
                return true;
            }
        }
        return false;
    }
    private boolean isWearingIron(Player player){
        PlayerInventory inv = player.getInventory();
        ItemStack helmet = inv.getHelmet();
        ItemStack chest = inv.getChestplate();
        ItemStack legs = inv.getLeggings();
        ItemStack boots = inv.getLeggings();
        if (helmet != null){
            if (helmet.getType() == Material.IRON_HELMET){
                return true;
            }
        }
        if (chest != null){
            if (chest.getType() == Material.IRON_CHESTPLATE){
                return true;
            }
        }
        if (legs != null){
            if (legs.getType() == Material.IRON_LEGGINGS){
                return true;
            }
        }
        if (boots != null){
            if (boots.getType() == Material.IRON_BOOTS){
                return true;
            }
        }
        return false;
    }
    private boolean isWearingChains(Player player){
        PlayerInventory inv = player.getInventory();
        ItemStack helmet = inv.getHelmet();
        ItemStack chest = inv.getChestplate();
        ItemStack legs = inv.getLeggings();
        ItemStack boots = inv.getLeggings();
        if (helmet != null){
            if (helmet.getType() == Material.CHAINMAIL_HELMET){
                return true;
            }
        }
        if (chest != null){
            if (chest.getType() == Material.CHAINMAIL_CHESTPLATE){
                return true;
            }
        }
        if (legs != null){
            if (legs.getType() == Material.CHAINMAIL_LEGGINGS){
                return true;
            }
        }
        if (boots != null){
            if (boots.getType() == Material.CHAINMAIL_BOOTS){
                return true;
            }
        }
        return false;
    }
}
