package com.aeonblox.RaceMechanics.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetEvent;

import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class EntityTargetPlayerEvent implements Listener{
	Utils utils;
	public EntityTargetPlayerEvent(Utils utils){
		this.utils = utils;
	}
	@EventHandler
	public void target(EntityTargetEvent e){
		if (e.getTarget() instanceof Player){
			if (utils.getPlayerRace(e.getTarget().getUniqueId()) == RaceType.Wraith || utils.getPlayerRace(e.getTarget().getUniqueId()) == RaceType.Demon){
				e.setCancelled(true);
			}
		}
	}
}
