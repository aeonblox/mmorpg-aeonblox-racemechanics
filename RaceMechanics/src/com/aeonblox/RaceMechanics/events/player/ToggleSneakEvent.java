package com.aeonblox.RaceMechanics.events.player;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.HealthMechanics.HealthMechanics;
import java.util.Collection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.utils.Utils;
import com.aeonblox.rpg.PermissionMechanics.PermissionMechanics;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ToggleSneakEvent implements Listener{
	Utils utils;
	public ToggleSneakEvent(Utils utils){
		this.utils = utils;
	}

	public boolean isVanished = false;

	@EventHandler
	public void sneak(final PlayerToggleSneakEvent e){
		if (utils.getBonusRace(e.getPlayer().getUniqueId()) == BonusRaceType.Assassin) {
			if (!(HealthMechanics.in_combat.containsKey(e.getPlayer().getName()))) {
				Location loc = e.getPlayer().getLocation();
				for (int b = 0; b < 2; b++) {
					for (int z = 0; z < 9; z++) {
						e.getPlayer().getWorld().playEffect(loc, Effect.SMOKE, z);
					}
					loc.setY(loc.getY() + 1D);
				}
				Bukkit.getScheduler().runTaskLater(utils.getPlugin(), new Runnable() {
					public void run() {
						if (e.getPlayer().isSneaking()) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								if (!PermissionMechanics.isStaff(p)) {
									p.hidePlayer(e.getPlayer());
									isVanished = true;
								}
							}
							if (e.getPlayer().hasPotionEffect(PotionEffectType.SPEED)) {
								Collection<PotionEffect> pe = e.getPlayer().getActivePotionEffects();
								for (PotionEffect effect : pe) {
									if (effect.getType().equals(PotionEffectType.SPEED)) {
										if (effect.getAmplifier() != 8) {
											e.getPlayer().removePotionEffect(PotionEffectType.SPEED);
											e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 8), true);

										}
									}
								}
							}
							if (!e.getPlayer().hasPotionEffect(PotionEffectType.SPEED)) {
								e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 8), true);
							}
						} else {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.showPlayer(e.getPlayer());
								isVanished = false;
							}

							if (e.getPlayer().hasPotionEffect(PotionEffectType.SPEED)) {
								Collection<PotionEffect> pe = e.getPlayer().getActivePotionEffects();
								for (PotionEffect effect : pe) {
									if (effect.getType().equals(PotionEffectType.SPEED)) {
										if (effect.getAmplifier() == 8) {
											Aeonblox.getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(Aeonblox.getPlugin(), new Runnable() {
												public void run() {
													e.getPlayer().removePotionEffect(PotionEffectType.SPEED);
												}
											}, 3L);
										}
									}
								}
							}
						}
					}
				}, 2L);
			}else{
				e.getPlayer().sendMessage(ChatColor.DARK_GRAY + "You fail to disappear...");
				e.getPlayer().sendMessage(ChatColor.DARK_GRAY + "Try again when you are out of combat!");
			}
		}
	}

	@EventHandler
	public void forceOut(final EntityDamageByEntityEvent e) {
		if (!(e.getEntity() instanceof Player)) {
			 return;
		}

		if (utils.getBonusRace(e.getEntity().getUniqueId()) == BonusRaceType.Assassin) {

			if(isVanished == true) {
				for (Player p: Bukkit.getOnlinePlayers()) {
					p.showPlayer((Player) e.getEntity());
				}
				if(((Player) e.getEntity()).isSneaking()) {
					((Player) e.getEntity()).setSneaking(false);
				}
				if(((Player) e.getEntity()).hasPotionEffect(PotionEffectType.SPEED)) {
					((Player) e.getEntity()).removePotionEffect(PotionEffectType.SPEED);
				}
			}
		}
	}
}
