package com.aeonblox.RaceMechanics.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class RespawnEvent implements Listener{
	Utils utils;
	public RespawnEvent(Utils ut){
		this.utils = ut;
	}
	@EventHandler
	public void respawn(PlayerRespawnEvent e){
		Player player = e.getPlayer();
		RaceType type = utils.getPlayerRace(player.getUniqueId());
		if (type == RaceType.Demon){
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1));
			player.setMaxHealth(player.getMaxHealth() * 1.05);
		}
		else if (type == RaceType.Wraith){
			player.setMaxHealth(20D * 1.25);
		} else if (type== RaceType.Nymph){
			player.setMaxHealth(20D * 1.15);
		} else if (type == RaceType.Elf){
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
		} else {
			player.setMaxHealth(20D);
		}
		BonusRaceType bonusType = utils.getBonusRace(player.getUniqueId());
		if (bonusType == BonusRaceType.Paladin){
			player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, Integer.MAX_VALUE,2));
		}
		if (bonusType == BonusRaceType.Angel){
			player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2));
		}
	}
}
