package com.aeonblox.RaceMechanics.events.player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;
import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.CommunityMechanics.CommunityMechanics;
import java.util.Arrays;
import java.util.List;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;

public class JoinEvent implements Listener{
	public JoinEvent(RMMain instance, Utils utils){
		plugin = instance;
		this.utils = utils;
	}
	RMMain plugin;
	private static Utils utils;
	@EventHandler (priority = EventPriority.MONITOR)
	public void join(final PlayerJoinEvent e){

		for (PotionEffectType t: PotionEffectType.values()){
			try {
				if (e.getPlayer().hasPotionEffect(t)){
					e.getPlayer().removePotionEffect(t);
				}
			} catch(Exception ex){

			}
		}
		utils.createPlayer(e.getPlayer().getUniqueId());
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Aeonblox.getPlugin(), new Runnable() {
			public void run() {
				updateRaceDial(e.getPlayer());
			}
		}, 60L);
	}

	//Todo: Add a cooldown to this to prevent - lag
	@EventHandler
	public void onInventoryOpenEvent(PlayerAchievementAwardedEvent e){
		final Player pl = (Player) e.getPlayer();
		if (utils.getPlayerRace(pl.getUniqueId()) != RaceType.Default) {
			if (e.getAchievement().equals(Achievement.OPEN_INVENTORY)) {
				e.setCancelled(true);
				//Bukkit.getScheduler().runTask(plugin, new Runnable() {
				//			public void run() {
				utils.createPlayer(pl.getUniqueId());
				updateRaceDial(pl);
				CommunityMechanics.updateCommBook(pl);
				//			}
				//});
				pl.sendMessage(ChatColor.RED + "Updating your Race Map...");
				pl.removeAchievement(Achievement.OPEN_INVENTORY);
				pl.updateInventory();
				return;
			}
		}
		if (e.getAchievement().equals(Achievement.OPEN_INVENTORY)) {
			updateRaceDial(pl);
			CommunityMechanics.updateCommBook(pl);
			e.setCancelled(true);
			pl.removeAchievement(Achievement.OPEN_INVENTORY);
		}
	}
/*
*
*             p.playSound(p.getLocation(), Sound.ITEM_BREAK, 1.0F, 1.4F);
*             Use item Break for switching in the array list of abilities on the dial
*
*                 p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.25F);
 *                 Use this sound for successful ability activation?
*
 */
	public static void updateRaceDial(Player p) {
		int slot = -1;
		boolean book = false;
		for (ItemStack is : p.getInventory().getContents()) {
			slot++;
			is = p.getInventory().getItem(slot);
			if (is != null && isRaceDial(is)) {
				book = true;
				break; // Slot is what it should be.
			}
		}

		if (book == true) {
			p.getInventory().setItem(slot, generateRaceDial(p));
		}

		if (book == false) {
			if (p.getInventory().getItem(7) == null || p.getInventory().getItem(7).getType() == Material.AIR) {
				p.getInventory().setItem(7, generateRaceDial(p));
			} else {
				if (p.getInventory().firstEmpty() != -1) {
					p.getInventory().setItem(p.getInventory().firstEmpty(), generateRaceDial(p));
				}
			}
		}
		p.updateInventory();
	}

	public static boolean isRaceDial(ItemStack i) {
		try {
			if (i.getType() == Material.NETHER_STAR && i.hasItemMeta()) {
				if (i.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW.toString() + ChatColor.BOLD.toString() + "Race Dial")) {
					return true;
				}
			}
		} catch (Exception e) {
			return false;
		}

		return false;
	}

	public static ItemStack generateRaceDial(Player p) {
		if (!(p.getInventory().contains(Material.NETHER_STAR))) {
			return createCustomItem(new ItemStack(Material.NETHER_STAR), ChatColor.YELLOW.toString() + ChatColor.BOLD.toString() + "Race Dial", Arrays.asList(ChatColor.GRAY
					+ "Embodiment of your soul.", ChatColor.GRAY + "Right-click with this in hand to", ChatColor.GRAY + "cycle through your Race's abilities.", ChatColor.GREEN
					+ "Race: " + getPlayerRace(p), ChatColor.GOLD + "Bonus Race: " + getPlayerBonusRace(p)));
		}
		ItemStack raceDial = createCustomItem(new ItemStack(Material.NETHER_STAR), ChatColor.YELLOW.toString() + ChatColor.BOLD.toString() + "Race Dial",
				Arrays.asList(ChatColor.GRAY + "Embodiment of your soul..", ChatColor.GRAY + "Right-click with this in hand to", ChatColor.GRAY + "cycle through your Race's abilities.",
						ChatColor.GREEN + "Race: " + getPlayerRace(p), ChatColor.GOLD + "Bonus Race: " + getPlayerBonusRace(p)));

		return raceDial;
	}

	public static ItemStack createCustomItem(ItemStack is, String name, List<String> lore) {
		ItemMeta im = is.getItemMeta();
		if (name != null) {
			im.setDisplayName(name);
		}
		if (lore != null) {
			im.setLore(lore);
		}
		is.setItemMeta(im);
		return is;
	}

	public static String getPlayerRace(Player p) {
		String s = ChatColor.GRAY + "Non-Existant";
		if(utils.getPlayerRace(p.getUniqueId()) == RaceType.Elf) {
			s = ChatColor.GREEN + "Elf";
		}else
		if(utils.getPlayerRace(p.getUniqueId()) == RaceType.Wraith) {
			s = ChatColor.DARK_GRAY + "Wraith";
		}else
		if(utils.getPlayerRace(p.getUniqueId()) == RaceType.Demon) {
			s = ChatColor.RED + "Demon";
		}else
		if(utils.getPlayerRace(p.getUniqueId()) == RaceType.Priest) {
			s = ChatColor.YELLOW + "Priest";
		}else
		if(utils.getPlayerRace(p.getUniqueId()) == RaceType.Nymph) {
			s = ChatColor.BLUE + "Nymph";
		}else
		if(utils.getPlayerRace(p.getUniqueId()) == RaceType.Chameleon) {
			s = ChatColor.DARK_PURPLE + "Chameleon";
		}else
		if(utils.getPlayerRace(p.getUniqueId()) == RaceType.Human) {
			s = ChatColor.WHITE + "Human";
		}

		return s;
	}

	public static String getPlayerBonusRace(Player p) {
		String s = ChatColor.GRAY + "Non-Existant";
		if(utils.getBonusRace(p.getUniqueId()) == BonusRaceType.Paladin) {
			s = ChatColor.YELLOW + "Paladin";
		}else
		if(utils.getBonusRace(p.getUniqueId()) == BonusRaceType.Assassin) {
			s = ChatColor.DARK_GRAY + "Assassin";
		}else
		if(utils.getBonusRace(p.getUniqueId()) == BonusRaceType.Angel) {
			s = ChatColor.WHITE + "Angel";
		}

		return s;
	}
}
