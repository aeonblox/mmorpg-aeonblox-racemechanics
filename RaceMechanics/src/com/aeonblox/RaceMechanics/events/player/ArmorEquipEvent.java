package com.aeonblox.RaceMechanics.events.player;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class ArmorEquipEvent implements Listener{
	Utils utils;
	public ArmorEquipEvent(Utils utils){
		this.utils = utils;
	}
	@EventHandler
	public void equip(InventoryClickEvent e){
		Player player = (Player) e.getWhoClicked();
		UUID uuid = player.getUniqueId();
		boolean bool = checkArmor(uuid, player.getInventory().getHelmet()) || checkArmor(uuid, player.getInventory().getBoots()) || checkArmor(uuid, player.getInventory().getLeggings()) || checkArmor(uuid, player.getInventory().getChestplate());
		e.setCancelled(bool);
		if (bool){
			player.sendMessage(ChatColor.RED + "You can't equip this kind of armor!");
			if (player.getInventory().getHelmet() != null){
				player.getInventory().addItem(player.getInventory().getHelmet());
				player.getInventory().setHelmet(null);
			} else if (player.getInventory().getChestplate() != null){
				player.getInventory().addItem(player.getInventory().getChestplate());
				player.getInventory().setChestplate(null);
			}
			else if (player.getInventory().getLeggings() != null){
				player.getInventory().addItem(player.getInventory().getLeggings());
				player.getInventory().setLeggings(null);
			}
			else if (player.getInventory().getBoots() != null){
				player.getInventory().addItem(player.getInventory().getBoots());
				player.getInventory().setBoots(null);
			}		
		}
	}
	@EventHandler
	public void interact(PlayerInteractEvent e){
		Player player = e.getPlayer();
		UUID uuid = player.getUniqueId();
		boolean bool = checkArmor(uuid, player.getInventory().getHelmet()) || checkArmor(uuid, player.getInventory().getBoots()) || checkArmor(uuid, player.getInventory().getLeggings()) || checkArmor(uuid, player.getInventory().getChestplate());
		e.setCancelled(bool);
		if (bool){
			player.sendMessage(ChatColor.RED + "You can't equip this kind of armor!");
			if (player.getInventory().getHelmet() != null){
				player.getInventory().addItem(player.getInventory().getHelmet());
				player.getInventory().setHelmet(null);
			} else if (player.getInventory().getChestplate() != null){
				player.getInventory().addItem(player.getInventory().getChestplate());
				player.getInventory().setChestplate(null);
			}
			else if (e.getPlayer().getInventory().getLeggings() != null){
				player.getInventory().addItem(player.getInventory().getLeggings());
				player.getInventory().setLeggings(null);
			}
			else if (player.getInventory().getBoots() != null){
				player.getInventory().addItem(player.getInventory().getBoots());
				player.getInventory().setBoots(null);
			}		
		}
	}
	public boolean checkArmor(UUID player, ItemStack item){
		boolean toReturn = false;
		RaceType type = utils.getPlayerRace(player);
		if (type == RaceType.Titan && isGoldenArmor(item)){
			toReturn = true;
		}
		else if (type == RaceType.Wraith && isChainMailArmor(item)){
			toReturn = true;
		}
		else if (type == RaceType.Demon && isGoldenArmor(item)){
			toReturn = true;
		} else if (type == RaceType.Elf && isDiamondArmor(item)){
			toReturn = true;
		}
		return toReturn;
	}
	public boolean isGoldenArmor(ItemStack item){
		if (item != null){
			if (item.getType() == Material.GOLD_BOOTS || item.getType() == Material.GOLD_CHESTPLATE || item.getType() == Material.GOLD_LEGGINGS || item.getType() == Material.GOLD_HELMET){
				return true;
			}
		}
		return false;
	}
	public boolean isDiamondArmor(ItemStack item){
		if (item != null){
			if (item.getType() == Material.DIAMOND_BOOTS || item.getType() == Material.DIAMOND_CHESTPLATE || item.getType() == Material.DIAMOND_LEGGINGS || item.getType() == Material.DIAMOND_HELMET){
				return true;
			}
		}
		return false;
	}
	public boolean isChainMailArmor(ItemStack item){
		if (item != null){
			if (item.getType() == Material.CHAINMAIL_BOOTS || item.getType() == Material.CHAINMAIL_CHESTPLATE || item.getType() == Material.CHAINMAIL_LEGGINGS || item.getType() == Material.CHAINMAIL_HELMET){
				return true;
			}
		}
		return false;
	}
}
