package com.aeonblox.RaceMechanics.events.player;

import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.commands.PBrightCommand;
import com.aeonblox.RaceMechanics.commands.PhealCommand;
import com.aeonblox.RaceMechanics.events.clickAndBreak.AirClickEvent;
import com.aeonblox.RaceMechanics.utils.Utils;

public class LeaveEvent implements Listener{
	RMMain plugin;
	Utils utils;
	public LeaveEvent(RMMain instance, Utils utils){
		plugin = instance;
		this.utils = utils;
	}
	@EventHandler
	public void quit(PlayerQuitEvent e){
		utils.saveRacePlayer(utils.getRacePlayer(e.getPlayer().getUniqueId()), true);
		UUID uuid = e.getPlayer().getUniqueId();
		plugin.configHandler.saveCustomConfig(plugin.RacePlayerConfig);
		if (plugin.pfirePlayers.contains(uuid)){
			plugin.pfirePlayers.remove(uuid);
		}
		if (PBrightCommand.fullbright.contains(uuid)){
			PBrightCommand.fullbright.remove(uuid);
		}
		if (AirClickEvent.hasShotFireBall.contains(uuid)){
			AirClickEvent.hasShotFireBall.remove(uuid);
		}
		if (PhealCommand.phealPlayers.containsKey(uuid)){
			PhealCommand.phealPlayers.remove(uuid);
		}
		if (PhealCommand.phealOtherPlayers.containsKey(uuid)){
			PhealCommand.phealOtherPlayers.remove(uuid);
		}
	}
}
