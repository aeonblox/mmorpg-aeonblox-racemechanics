package com.aeonblox.RaceMechanics.events;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.aeonblox.RaceMechanics.utils.Utils;

public class ChatEvent implements Listener{
	Utils utils;
	public ChatEvent(Utils utils){
		this.utils = utils;
	}
	@EventHandler (priority = EventPriority.HIGHEST)
	public void chat(AsyncPlayerChatEvent e){
		UUID uuid = e.getPlayer().getUniqueId();
		if (!e.getPlayer().hasPermission("RaceMechanics.bypasschat")){
			if (utils.hasBonusRace(uuid)){
				e.getPlayer().setDisplayName(utils.getPlayerRace(uuid).getNameColor() + e.getPlayer().getName() + "" + utils.getBonusRace(uuid).getSuffix());
			} else {
				e.getPlayer().setDisplayName(utils.getPlayerRace(uuid).getNameColor() + e.getPlayer().getName() + ChatColor.RESET);
			}
		}
	}
}
