package com.aeonblox.RaceMechanics.events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class WaterEvent implements Listener{
	private Utils utils;
	public WaterEvent(Utils utils){
		this.utils = utils;
	}
	@EventHandler
	public void waterdamage(EntityDamageEvent e){
		if (e.getCause() == DamageCause.DROWNING){
			if (e.getEntity() instanceof Player){
				if (utils.getPlayerRace(e.getEntity().getUniqueId()) ==  RaceType.Nymph){
					e.setCancelled(true);
				}
			}
		}
	}
	@EventHandler
	public void doubleDamage(EntityDamageByEntityEvent e){
		if (e.getEntity() instanceof Player){
			if (utils.getPlayerRace(e.getEntity().getUniqueId()) ==  RaceType.Nymph){
				Material m = e.getEntity().getLocation().getBlock().getType();
				if (m == Material.STATIONARY_WATER || m == Material.WATER){
					if (!e.getEntity().isDead()){
						e.setDamage(e.getDamage()*0.5D);
					}
				} 
			}
			if (utils.getPlayerRace(e.getDamager().getUniqueId()) ==  RaceType.Nymph){
				Material m = e.getEntity().getLocation().getBlock().getType();
				if (m == Material.STATIONARY_WATER || m == Material.WATER){
					e.setDamage(e.getDamage()*2);
				}
			}
		}
	}
}
