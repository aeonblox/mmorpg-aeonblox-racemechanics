package com.aeonblox.RaceMechanics.events.clickAndBreak;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.RacePlayer;
import com.aeonblox.RaceMechanics.utils.Utils;

public class BowPullBackEvent implements Listener {
	Utils utils;
	public BowPullBackEvent(Utils utils, RMMain instance){
		this.utils = utils;
		this.plugin = instance;
	}
	public int getIndex(String s, List<String> list){
		int counter = 0;
		for (String sa:list){
			if (sa.contains(s)){
				return counter;
			}
			counter++;
		}
		return -1;
	}
	RMMain plugin;
	ArrayList<UUID> bowShot = new ArrayList<UUID>();
	@EventHandler
	public void bowShoot(EntityShootBowEvent e){
		if (e.getEntity() instanceof Player){
			if (utils.getPlayerRace(e.getEntity().getUniqueId()) == RaceType.Elf){
				e.setCancelled(true);
			}
		}
	}
	@EventHandler 
	public void bowPullBack(final PlayerInteractEvent e){
		Player player = e.getPlayer();
		double random = utils.randInt(0, 3);
		random = random/10;
		long seconds = 10L;
		if (player.getItemInHand().getType() == Material.BOW){
			ItemStack bow = e.getPlayer().getItemInHand();
			if (bow.getItemMeta() != null && bow.getItemMeta().getLore() != null){
				if (getIndex("HPS: ", bow.getItemMeta().getLore()) != -1){
					String s = bow.getItemMeta().getLore().get(getIndex("HPS: ", bow.getItemMeta().getLore()));
					String s2 = s.split(" ")[1].replace("/s", "");
					seconds = Long.valueOf(s2) * 20;

				}
			}
			RacePlayer p = utils.getRacePlayer(player.getUniqueId());
			if (p.getRace() == RaceType.Elf){
				if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK){
					if (e.getPlayer().getInventory().containsAtLeast(new ItemStack(Material.ARROW), 1)){
						e.getPlayer().getInventory().removeItem(new ItemStack(Material.ARROW));
						bowShot.add(e.getPlayer().getUniqueId());
						e.getPlayer().getInventory().remove(new ItemStack(Material.ARROW, 1));
						final Arrow arrow = (Arrow)player.getWorld().spawn(player.getEyeLocation(), Arrow.class);
						arrow.setShooter(player);
						arrow.setVelocity(player.getEyeLocation().getDirection().multiply(2.5));
						e.setCancelled(true);
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable(){public void run(){bowShot.remove(bowShot.indexOf(e.getPlayer().getUniqueId()));}}, seconds);
					}
				} else if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
					if (!bowShot.contains(e.getPlayer().getUniqueId())){

						if (e.getPlayer().getInventory().containsAtLeast(new ItemStack(Material.ARROW), 1)){
							e.getPlayer().getInventory().removeItem(new ItemStack(Material.ARROW));
							final Arrow arrow = (Arrow)player.getWorld().spawn(player.getEyeLocation(), Arrow.class);
							arrow.setShooter(player);
							arrow.setVelocity(player.getEyeLocation().getDirection().multiply(2.5));
							final Arrow arrow2 = (Arrow)player.getWorld().spawn(player.getEyeLocation(), Arrow.class);
							arrow2.setShooter(player);
							arrow2.setVelocity(player.getEyeLocation().getDirection().multiply(2.5).subtract(new Vector(random, 0, 0)));
							final Arrow arrow3 = (Arrow)player.getWorld().spawn(player.getEyeLocation(), Arrow.class);
							arrow3.setShooter(player);
							arrow3.setVelocity(player.getEyeLocation().getDirection().multiply(2.5).subtract(new Vector(0, 0, random)));
							e.setCancelled(true);
							bowShot.add(e.getPlayer().getUniqueId());
							Bukkit.getScheduler().runTaskLater(plugin, new Runnable(){public void run(){bowShot.remove(bowShot.indexOf(e.getPlayer().getUniqueId()));}}, seconds);

						}
					}
				}
			}
		}
	}
}
