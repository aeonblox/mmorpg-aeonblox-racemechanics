package com.aeonblox.RaceMechanics.events.clickAndBreak;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.inventory.Inventories;
import com.aeonblox.RaceMechanics.utils.RacePlayer;
import com.aeonblox.RaceMechanics.utils.Utils;

public class RacePaneClick implements Listener{
	Utils utils;
	public RacePaneClick(Utils u){
		this.utils = u;
	}
	@EventHandler
	public void click(BlockPlaceEvent e){
		if (e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().hasItemMeta() && e.getPlayer().getItemInHand().getItemMeta().getLore() != null && e.getPlayer().getItemInHand().getItemMeta().getDisplayName() != null){
			ItemStack i = e.getPlayer().getItemInHand();
			Inventory inv = e.getPlayer().getInventory();
			RacePlayer player = utils.getRacePlayer(e.getPlayer().getUniqueId());
			if (i.equals(Inventories.p_p)){
				e.setCancelled(true);
				player.setRaceType(RaceType.Chameleon);
				inv.remove(Inventories.p_p);
				e.getPlayer().updateInventory();
			} else if (i.equals(Inventories.b_p)){
				e.setCancelled(true);
				player.setRaceType(RaceType.Nymph);
				inv.remove(Inventories.b_p);
				e.getPlayer().updateInventory();
			}
			else if (i.equals(Inventories.bl_p)){
				e.setCancelled(true);
				player.setRaceType(RaceType.Wraith);
				inv.remove(Inventories.bl_p);
				e.getPlayer().updateInventory();
			}
			else if (i.equals(Inventories.red_pane)){
				e.setCancelled(true);
				player.setRaceType(RaceType.Demon);
				inv.remove(Inventories.red_pane);
				e.getPlayer().updateInventory();
			}
			else if (i.equals(Inventories.y_p)){
				e.setCancelled(true);
				player.setRaceType(RaceType.Priest);
				inv.remove(Inventories.y_p);
				e.getPlayer().updateInventory();
			}
			else if (i.equals(Inventories.pl_p)){
				e.setCancelled(true);
				player.setRaceType(RaceType.Human);
				inv.remove(Inventories.pl_p);
				e.getPlayer().updateInventory();
			}
			else if (i.equals(Inventories.g_p)){
				e.setCancelled(true);
				player.setRaceType(RaceType.Elf);
				inv.remove(Inventories.g_p);
				e.getPlayer().updateInventory();
			} else if (i.equals(Inventories.gl_p)){
				e.setCancelled(true);
				player.setRaceType(RaceType.Titan);
				inv.remove(Inventories.gl_p);
				e.getPlayer().updateInventory();
			}
			utils.saveRacePlayer(utils.getRacePlayer(e.getPlayer().getUniqueId()));
		}
	}
}
