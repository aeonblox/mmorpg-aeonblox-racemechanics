package com.aeonblox.RaceMechanics.events.clickAndBreak;

import com.aeonblox.rpg.Utilities.ParticleEffect;
import java.util.Random;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class EntityInteract implements Listener{
	Utils utils;
	RMMain plugin;
	public EntityInteract(Utils utils, RMMain instance){
		this.utils = utils;
		this.plugin = instance;
	}
	@EventHandler
	public void entityTouch(PlayerInteractEntityEvent e){
		if (e.getRightClicked() instanceof Player){
			if (utils.getPlayerRace(e.getPlayer().getUniqueId()) == RaceType.Priest){
				if (e.getPlayer().isSneaking()){
					e.getPlayer().performCommand("pheal " + ((Player) e.getRightClicked()).getName());
					e.getRightClicked().sendMessage(ChatColor.GOLD + e.getPlayer().getName() + " has reinvigorated you!");
					ParticleEffect.HEART.display(new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(),
							1F, 5, e.getRightClicked().getLocation().add(0, 0.25, 0), 3D);
					//TODO pls fix
					//Iz not broken??
				}
			} 
		}
	}
}
