package com.aeonblox.RaceMechanics.events.clickAndBreak;

import com.aeonblox.RaceMechanics.events.player.JoinEvent;
import com.aeonblox.rpg.Utilities.ParticleEffect;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.SmallFireball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class AirClickEvent implements Listener{
	RMMain plugin;
	Utils utils;
	ArrayList<UUID> firiedPlayers = new ArrayList<UUID>();
	public AirClickEvent(RMMain instance, Utils utils){
		plugin = instance;
		this.utils = utils;
	}
	public static ArrayList<String> hasShotFireBall = new ArrayList<String>();
	@EventHandler
	public void click(final PlayerInteractEvent e){
		if (!(hasShotFireBall.contains(e.getPlayer().getName()))){
			if (plugin.pfirePlayers.contains(utils.getRacePlayer(e.getPlayer().getUniqueId()))){
				e.getPlayer().launchProjectile(SmallFireball.class);
				if (!e.getPlayer().hasPermission("aeonrpg.fireball") || !e.getPlayer().hasPermission("aeonrpg.admin")){
					hasShotFireBall.add(e.getPlayer().getName());
					Bukkit.getScheduler().runTaskLater(plugin, new Runnable(){
						public void run(){
							hasShotFireBall.remove(hasShotFireBall.indexOf(e.getPlayer().getName()));
						}
					}, 20L);
				}
			}
		} else {
			e.getPlayer().sendMessage(plugin.getMessage("cantShootArrows"));
		}
		if(JoinEvent.isRaceDial(e.getPlayer().getItemInHand())) {
			if (utils.getPlayerRace(e.getPlayer().getUniqueId()) == RaceType.Priest && (e.getPlayer().getItemInHand() == null || e.getPlayer().getItemInHand().getType() == Material.AIR) && e.getClickedBlock() != null && e.getClickedBlock().getType() != Material.SIGN_POST && e.getClickedBlock().getType() != Material.SIGN) {
				e.getPlayer().performCommand("pheal");
				ParticleEffect.HEART.display(new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(),
						1F, 5, e.getPlayer().getLocation().add(0, 0.25, 0), 3D);
			}
		}
		if (utils.getBonusRace(e.getPlayer().getUniqueId()) == BonusRaceType.Assassin){
			if (!firiedPlayers.contains(e.getPlayer().getUniqueId())){
				if (e.getPlayer().getItemInHand().getType() == Material.FLINT_AND_STEEL){
					firiedPlayers.add(e.getPlayer().getUniqueId());
					final Arrow arrow = (Arrow)e.getPlayer().getWorld().spawn(e.getPlayer().getEyeLocation(), Arrow.class);
					arrow.setShooter(e.getPlayer());
					arrow.setVelocity(e.getPlayer().getEyeLocation().getDirection().multiply(2.5));
					arrow.setFireTicks(20000);
					Bukkit.getScheduler().runTaskLater(plugin, new Runnable(){ public void run(){
						firiedPlayers.remove(firiedPlayers.indexOf(e.getPlayer().getUniqueId()));
					}}, 20L);
				}
			}
		}
	}
}
