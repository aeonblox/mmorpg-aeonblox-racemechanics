package com.aeonblox.RaceMechanics.events.clickAndBreak;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class BlockBreak implements Listener{
	Utils utils;
	public BlockBreak(Utils utils){
		this.utils = utils;
	}
	@EventHandler
	public void blockBreak(BlockBreakEvent e){
		if (utils.getPlayerRace(e.getPlayer().getUniqueId()) == RaceType.Demon){
			if (e.getBlock().getType().name().toLowerCase().contains("ore")){
				e.getBlock().getDrops().clear();
				if (e.getBlock().getType() == Material.GOLD_ORE){
					e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT, 2));
				}
				else if (e.getBlock().getType() == Material.IRON_ORE){
					e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT, 1));
				}
			}
		}
	}
}
