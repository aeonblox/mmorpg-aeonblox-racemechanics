package com.aeonblox.RaceMechanics.events.inventory;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.inventory.Inventories;
import com.aeonblox.RaceMechanics.utils.RacePlayer;
import com.aeonblox.RaceMechanics.utils.Utils;

public class InventoryClick implements Listener{
	RMMain plugin;
	Inventories invs;
	Utils utils;
	public InventoryClick(RMMain instance, Inventories invse, Utils utils){
		this.utils = utils;
		plugin = instance;
		invs = invse;
	}
	@EventHandler
	public void interact(PlayerInteractEvent e){
		if (e.getClickedBlock() != null && (e.getClickedBlock().getType().equals(Material.SIGN) || e.getClickedBlock().getType().equals(Material.SIGN_POST)) && e.getAction() == Action.RIGHT_CLICK_BLOCK){
			Inventory inv = invs.chooseMenu();
			Sign s = (Sign) e.getClickedBlock().getState();
			if (s.getLine(0).equalsIgnoreCase("[Race]")){
				if (utils.getPlayerRace(e.getPlayer().getUniqueId()) == RaceType.Default){
					e.getPlayer().openInventory(inv);
				} else {
					if (!e.getPlayer().hasPermission("RaceMechanics.bypasschooseblock")){
						e.getPlayer().sendMessage(plugin.getMessage("alreadyChoseRace"));
						e.setCancelled(true);
					} else {
						e.getPlayer().openInventory(inv);
					}
				}
			}
		} 
	}

	@EventHandler
	public void click(InventoryClickEvent e){
		if (e.getInventory().getName().equals(ChatColor.GOLD + "Choose your race!")){
			e.setCancelled(true);
			RacePlayer player = utils.getRacePlayer(e.getWhoClicked().getUniqueId());
			if (e.getCurrentItem().equals(Inventories.p_p)){
				player.setRaceType(RaceType.Chameleon);
				((Player)e.getWhoClicked()).updateInventory();
			} else if (e.getCurrentItem().equals(Inventories.b_p)){
				((Player)e.getWhoClicked()).updateInventory();
				player.setRaceType(RaceType.Nymph);
			}
			else if (e.getCurrentItem().equals(Inventories.bl_p)){
				player.setRaceType(RaceType.Wraith);
				((Player)e.getWhoClicked()).updateInventory();
			}
			else if (e.getCurrentItem().equals(Inventories.red_pane)){
				player.setRaceType(RaceType.Demon);
				((Player)e.getWhoClicked()).updateInventory();
			}
			else if (e.getCurrentItem().equals(Inventories.y_p)){
				player.setRaceType(RaceType.Priest);
				((Player)e.getWhoClicked()).updateInventory();
			}
			else if (e.getCurrentItem().equals(Inventories.pl_p)){
				player.setRaceType(RaceType.Human);
				((Player)e.getWhoClicked()).updateInventory();
			}
			else if (e.getCurrentItem().equals(Inventories.g_p)){
				player.setRaceType(RaceType.Elf);
				((Player)e.getWhoClicked()).updateInventory();
			}
			e.getWhoClicked().closeInventory();
			utils.saveRacePlayer(utils.getRacePlayer(e.getWhoClicked().getUniqueId()));
		}
	}
}
