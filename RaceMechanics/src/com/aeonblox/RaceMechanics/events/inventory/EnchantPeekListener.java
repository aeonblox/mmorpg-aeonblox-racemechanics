package com.aeonblox.RaceMechanics.events.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import net.minecraft.server.v1_8_R3.EnchantmentManager;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class EnchantPeekListener
implements Listener
{
	private Map<Player, Map<Integer, net.minecraft.server.v1_8_R3.ItemStack>> storage = new HashMap<Player, Map<Integer, net.minecraft.server.v1_8_R3.ItemStack>>();

	private static final Map<String, String> ENames = new HashMap<String, String>();
	Utils utils;
	public EnchantPeekListener(RMMain plugin, Utils utils)
	{
		this.utils = utils;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPrepareItemEnchantEvent(PrepareItemEnchantEvent evt) {
		Player p = evt.getEnchanter();
		if (utils.getPlayerRace(evt.getEnchanter().getUniqueId()) == RaceType.Wraith || utils.getPlayerRace(evt.getEnchanter().getUniqueId()) == RaceType.Elf){
			Map<Integer, net.minecraft.server.v1_8_R3.ItemStack> enchantments = new HashMap<Integer, net.minecraft.server.v1_8_R3.ItemStack>();
			List<String> lore = new ArrayList<String>();

			for (int cost : evt.getExpLevelCostsOffered()) {
				enchantments.put(Integer.valueOf(cost), makeEnchantedItemStack(evt.getItem(), cost, p));
				StringBuilder ex = new StringBuilder(new StringBuilder().append(cost).append(" -> ").toString());
				org.bukkit.inventory.ItemStack item = CraftItemStack.asBukkitCopy((net.minecraft.server.v1_8_R3.ItemStack)enchantments.get(Integer.valueOf(cost)));
				if (item.getType() == Material.ENCHANTED_BOOK) {
					Iterator<Entry<Enchantment, Integer>> i$ = ((EnchantmentStorageMeta)item.getItemMeta()).getStoredEnchants().entrySet().iterator(); if (i$.hasNext()) { Entry<Enchantment, Integer> x = (Entry<Enchantment, Integer>)i$.next();
					ex.append(new StringBuilder().append(", ").append(lookup(((Enchantment)x.getKey()).getName())).append(" ").append(x.getValue()).toString()); }
				}
				else
				{
					for (Entry<Enchantment, Integer> x : item.getEnchantments().entrySet()) {
						ex.append(new StringBuilder().append(", ").append(lookup(((Enchantment)x.getKey()).getName())).append(" ").append(x.getValue()).toString());
					}
				}
				lore.add(ex.toString().replace("> ,", "> "));
				ItemMeta meta = evt.getItem().getItemMeta();
				if (meta.hasLore()) {
					List<String> oldlore = cleanLore(meta.getLore());
					oldlore.addAll(lore);
					lore = oldlore;
				}
				meta.setLore(lore);
				evt.getItem().setItemMeta(meta);
				this.storage.put(evt.getEnchanter(), enchantments);
			}
		}
	}

	@EventHandler
	public void onEnchantItemEvent(EnchantItemEvent evt) {
		Player p = evt.getEnchanter();
		if (!p.hasPermission("enchantpeek.allow")) return;
		if (this.storage.containsKey(p)) {
			Map<Integer, net.minecraft.server.v1_8_R3.ItemStack> playerEnchants = (Map<Integer, net.minecraft.server.v1_8_R3.ItemStack>)this.storage.get(p);
			org.bukkit.inventory.ItemStack enchantedItem = CraftItemStack.asBukkitCopy((net.minecraft.server.v1_8_R3.ItemStack)playerEnchants.get(Integer.valueOf(evt.getExpLevelCost())));
			Map<Enchantment, Integer> correctEnchantments = enchantedItem.getEnchantments();
			if (enchantedItem.getType() == Material.ENCHANTED_BOOK) {
				correctEnchantments = ((EnchantmentStorageMeta)enchantedItem.getItemMeta()).getStoredEnchants();
			}
			Map<Enchantment, Integer> fixit = evt.getEnchantsToAdd(); fixit.clear();
			fixit.putAll(correctEnchantments);
			this.storage.remove(p);
		}
	}

	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent evt) {
		Player p = (Player)evt.getWhoClicked();
		if (!p.hasPermission("enchantpeek.allow")) return;
		if (evt.getInventory().getType().equals(InventoryType.ENCHANTING)) {
			org.bukkit.inventory.ItemStack item = evt.getCurrentItem();
			if ((item != null) && (item.hasItemMeta()) && (item.getItemMeta().hasLore())) {
				List<String> lore = cleanLore(item.getItemMeta().getLore());
				if (lore.size() == 0) lore = null;
				ItemMeta m = item.getItemMeta();
				m.setLore(lore);
				item.setItemMeta(m);
				this.storage.remove(p);
			}
		}
	}

	private net.minecraft.server.v1_8_R3.ItemStack makeEnchantedItemStack(org.bukkit.inventory.ItemStack itemToEnchant, int cost, Player p)
	{
		net.minecraft.server.v1_8_R3.ItemStack temp = CraftItemStack.asNMSCopy(itemToEnchant);

		if (((cost > 0) && (temp != null) && (p.getLevel() >= cost)) || (p.getGameMode() == GameMode.CREATIVE)) {
			return EnchantmentManager.a(new Random(), temp, cost);
		}
		return temp;
	}

	private static List<String> cleanLore(List<String> lore) {
		List<String> newlore = new ArrayList<String>();
		for (int i = 0; i < lore.size(); i++) {
			if (!((String)lore.get(i)).matches("\\d\\d? -> .*")) {
				newlore.add(lore.get(i));
			}
		}
		return newlore;
	}

	private String lookup(String name) {
		if (ENames.containsKey(name)) return (String)ENames.get(name);
		return name;
	}
}