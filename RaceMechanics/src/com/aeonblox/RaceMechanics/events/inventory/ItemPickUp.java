package com.aeonblox.RaceMechanics.events.inventory;

import org.bukkit.entity.Arrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.utils.Utils;

public class ItemPickUp implements Listener{
	RMMain plugin;
	Utils utils;
	public ItemPickUp(RMMain instance, Utils utils){
		this.plugin = instance;
		this.utils = utils;
	}
	@EventHandler
	public void pickup(PlayerPickupItemEvent  e){
		if (e.getItem() instanceof Arrow){
			e.setCancelled(true);
		}
	}
}
