package com.aeonblox.RaceMechanics.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class PfireCommand implements CommandExecutor{
	public Utils utils;
	public RMMain plugin;
	public PfireCommand(RMMain instance, Utils utils){
		this.plugin = instance;
		this.utils = utils;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("pfire")){
			if (sender instanceof Player){
				Player p = (Player) sender;
				if (utils.getPlayerRace(p.getUniqueId()) == RaceType.Demon){
					if (!(plugin.pfirePlayers.contains(utils.getRacePlayer(p.getUniqueId())))){
						sender.sendMessage(plugin.getMessage("pfOn"));
						plugin.pfirePlayers.add(utils.getRacePlayer(p.getUniqueId()));
						return true;
					} else {
						sender.sendMessage(plugin.getMessage("pfOff"));
						plugin.pfirePlayers.remove(utils.getRacePlayer(p.getUniqueId()));
						return true;
					}
				} else {
					sender.sendMessage(plugin.getMessage("noRace").replace("%RACE%", "Demon"));
					return true;
				}
			}
			return true;
		}
		return false;
	}

}
