package com.aeonblox.RaceMechanics.commands;

import java.util.ConcurrentModificationException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.TitanType;
import com.aeonblox.RaceMechanics.inventory.Inventories;
import com.aeonblox.RaceMechanics.utils.RacePlayer;
import com.aeonblox.RaceMechanics.utils.Utils;

public class MainCommand implements CommandExecutor{
	Utils utils;
	RMMain plugin;
	Inventories invs;
	public MainCommand(RMMain instance, Utils utils, Inventories i){
		this.utils = utils;
		this.plugin = instance;
		this.invs = i;
	}
	public void log(String s){
		plugin.getLogger().info(s);
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("AeonRace")){
			if (sender.hasPermission("aeonrpg.admin")){
				if (args.length == 0){
					utils.sendHelp(sender);
					return true;
				} else if (args.length == 1){
					if (args[0].equalsIgnoreCase("help")){
						utils.sendHelp(sender);
						return true;
					} else if (args[0].equalsIgnoreCase("reload")){
						utils.reloadConfig();
						sender.sendMessage(plugin.getMessage("reloadedConfig"));
						return true;
					} else if (args[0].equalsIgnoreCase("listplayers")){
						for (RacePlayer p: plugin.RacePlayers){
							sender.sendMessage(p.getPlayer().getName());
						}
						return true;
					} else if (args[0].equalsIgnoreCase("showinfo") || args[0].equalsIgnoreCase("sh")){
						Player player = (Player) sender;
						sender.sendMessage(ChatColor.YELLOW + "Your race: " + ChatColor.GREEN + utils.getPlayerRace(player.getUniqueId()));
						sender.sendMessage(ChatColor.YELLOW + "Your bonusrace: " + ChatColor.GREEN + utils.getBonusRace(player.getUniqueId()));
						sender.sendMessage(ChatColor.YELLOW + "Your titan type: " + ChatColor.GREEN + utils.getTitanType(player.getUniqueId()));
						sender.sendMessage(ChatColor.YELLOW + "Your UUID: " + ChatColor.GREEN + player.getUniqueId());
						return true;
					}
					else {
						utils.sendUnknownCommand(sender);
						return true;
					}
				} else if (args.length == 3){
					if (args[0].equalsIgnoreCase("setrace") || args[0].equalsIgnoreCase("sr")){
						try {
							if (utils == null){
								System.out.println("its the utils : '9");
							}
							if (utils.getRacePlayer(args[1]) == null){
								System.out.println("its the raceplayer!");
							}
							if (utils.getRacePlayer(args[1]).getUUID() == null){
								System.out.println("its the UUID!");
							}
							Player p = Bukkit.getPlayer(utils.getRacePlayer(args[1]).getUUID());
							if (p == null){
								System.out.println("its the player :'(");
							}
							RaceType type = RaceType.forNameWithNPE(args[2]);
							if (type == null){
								sender.sendMessage(plugin.getMessage("raceNotExists"));
								return true;
							}
							if (type != RaceType.Titan){
								utils.getRacePlayer(args[1]).setTitanType(TitanType.None);
							}
							utils.getRacePlayer(args[1]).setRaceType(type);
							utils.saveRacePlayer(utils.getRacePlayer(args[1]));
							return true;
						} catch (Exception ex){
							sender.sendMessage(plugin.getMessage("notOnline"));
							if (ex instanceof ConcurrentModificationException ){
								System.out.println("CME");
							}
							if (ex instanceof NullPointerException){
								System.out.println("NPE");
								ex.printStackTrace();
							}
						}
					} else if (args[0].equalsIgnoreCase("setbonusRace") || args[0].equalsIgnoreCase("sbr")){
						try {
							Player p = Bukkit.getPlayer(utils.getRacePlayer(args[1]).getUUID());
							BonusRaceType type = BonusRaceType.forName(args[2]);
							if (type == BonusRaceType.None){
								if (args[2].equals("none")){
									utils.getRacePlayer(p.getUniqueId()).setBonusRaceType(BonusRaceType.None);
									plugin.configHandler.getCustomConfig(plugin.RacePlayerConfig).set("players." +p.getName()+".bonustype", "none");
									plugin.configHandler.saveCustomConfig(plugin.RacePlayerConfig);
									p.setDisplayName(utils.getPlayerRace(p.getUniqueId()).getNameColor() + p.getName() + ChatColor.RESET);
									utils.saveRacePlayer(utils.getRacePlayer(args[1]));
									return true;
								}
								sender.sendMessage(plugin.getMessage("brNotExists"));
								return true;
							}
							String s = type.getName();
							String name = sender.getName();
							p.sendMessage(plugin.getMessage("raceChanged").replace("%PLAYER%", name).replace("%RACE%", s));
							utils.getRacePlayer(p.getUniqueId()).setBonusRaceType(type);
							utils.saveRacePlayer(utils.getRacePlayer(args[1]));
							return true; 
						} catch (Exception ex){
							sender.sendMessage(plugin.getMessage("notOnline"));
							if (ex instanceof ConcurrentModificationException ){
								System.out.println("CME");
							}
							if (ex instanceof NullPointerException){
								System.out.println("NPE");
								ex.printStackTrace();
							}
						}
					} else if (args[0].equalsIgnoreCase("settitantype") || args[0].equalsIgnoreCase("stt")){
						try {
							Player p = Bukkit.getPlayer(utils.getRacePlayer(args[1]).getUUID());
							TitanType type = TitanType.forName(args[2]);
							if (type == TitanType.None){
								if (args[2].equals("none")){
									utils.getRacePlayer(p.getUniqueId()).setTitanType(TitanType.None);
									plugin.configHandler.getCustomConfig(plugin.RacePlayerConfig).set("players." +p.getName()+".titantype", "none");
									plugin.configHandler.saveCustomConfig(plugin.RacePlayerConfig);
									p.setDisplayName(utils.getPlayerRace(p.getUniqueId()).getNameColor() + p.getName() + ChatColor.RESET);
									utils.saveRacePlayer(utils.getRacePlayer(args[1]));
									return true;
								}
								sender.sendMessage(plugin.getMessage("ttNotExist"));
								return true;
							}
							if  (utils.getPlayerRace(p.getUniqueId()) != RaceType.Titan){
								sender.sendMessage(plugin.getMessage("playerNotTitan"));
								return true;
							}
							String s = type.getName();
							String name = sender.getName();
							p.sendMessage(plugin.getMessage("titanChanged").replace("%PLAYER%", name).replace("%TITAN%", s));
							utils.getRacePlayer(p.getUniqueId()).setTitanType(type);
							utils.saveRacePlayer(utils.getRacePlayer(args[1]));
							return true; 
						} catch (Exception ex){
							sender.sendMessage(plugin.getMessage("notOnline"));
							if (ex instanceof ConcurrentModificationException ){
								System.out.println("CME");
							}
							if (ex instanceof NullPointerException){
								System.out.println("NPE");
								ex.printStackTrace();
							}
						}
					} 
					else {
						utils.sendUnknownCommand(sender);
						return true;
					}
				} else if (args.length == 2){
					if (args[0].equalsIgnoreCase("giveracepane") || args[0].equalsIgnoreCase("grp")){
						Player player = (Player) sender;
						if (args[1].equalsIgnoreCase("demon")){
							player.getInventory().addItem(Inventories.red_pane);
						} else if (args[1].equalsIgnoreCase("wraith")){
							player.getInventory().addItem(Inventories.bl_p);
						} else if (args[1].equalsIgnoreCase("human")){
							player.getInventory().addItem(Inventories.pl_p);
						}
						else if (args[1].equalsIgnoreCase("elf")){
							player.getInventory().addItem(Inventories.g_p);
						}
						else if (args[1].equalsIgnoreCase("chameleon")){
							player.getInventory().addItem(Inventories.p_p);
						} else if (args[1].equalsIgnoreCase("priest")){
							player.getInventory().addItem(Inventories.y_p);
						}
						else if (args[1].equalsIgnoreCase("nymph")){
							player.getInventory().addItem(Inventories.b_p);
						} else if (args[1].equalsIgnoreCase("titan")){
							player.getInventory().addItem(Inventories.gl_p);
						}
					} else if (args[0].equalsIgnoreCase("openchooseinv") || args[0].equalsIgnoreCase("oci")){
						try {
							Player pl = null;
							for (Player p: Bukkit.getOnlinePlayers()){
								if (p.getName().equalsIgnoreCase(args[1])){
									pl = p;
								}
							}
							pl.openInventory(invs.chooseMenu());
						} catch (Exception ex){

						}
					}else {
						utils.sendUnknownCommand(sender);
						return true;
					}
				} else {
					utils.sendUnknownCommand(sender);
					return true;
				}
			}else {
				utils.sendUnknownCommand(sender);
				return true;
			}
			return true;
		}
		return false;
	}
}
