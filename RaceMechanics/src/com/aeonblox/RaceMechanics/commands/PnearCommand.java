package com.aeonblox.RaceMechanics.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;

public class PnearCommand implements CommandExecutor{
	RMMain plugin;
	public PnearCommand(RMMain plugin){
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("pnear")){
			if (sender instanceof Player){
				Player player = (Player) sender;
				if (plugin.utils.getBonusRace(player.getUniqueId()) == BonusRaceType.Assassin){
					Location ploc = player.getLocation();
					String s = ChatColor.GOLD + "Players near you:";
					for (Player p: Bukkit.getOnlinePlayers()){
						if (ploc.distance(p.getLocation()) <= 32){
							s = s + ChatColor.GOLD + "\n" + p.getName() + ", " + Math.floor(ploc.distance(p.getLocation())) + " blocks away";
						}
					}
					sender.sendMessage(s);
					return true;
				} else {
					player.sendMessage(plugin.getMessage("noRace").replace("%RACE%", "Assassin"));
					return true;
				}
			}
			return true;
		}
		return false;
	}
}
