package com.aeonblox.RaceMechanics.commands;

import com.aeonblox.rpg.HealthMechanics.HealthMechanics;
import com.aeonblox.rpg.managers.PlayerManager;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;

public class PhealCommand implements CommandExecutor{
	RMMain plugin;
	public PhealCommand(RMMain plugin){
		this.plugin = plugin;
	}
	public static HashMap<UUID, Integer> phealPlayers = new HashMap<UUID, Integer>();
	public static HashMap<UUID, Integer> phealOtherPlayers = new HashMap<UUID, Integer>();
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("pheal")){
			if (sender instanceof Player){
				Player p = (Player) sender;
				if (plugin.utils.getPlayerRace(((Player) sender).getUniqueId()) == RaceType.Priest){
					if (args.length == 0 || args.length > 1){
						if (!(phealPlayers.containsKey(p.getUniqueId()))){
							double max_hp = HealthMechanics.getMaxHealthValue(p.getName());
							double current_hp = HealthMechanics.getPlayerHP(p.getName());
							if(current_hp + 1 > max_hp) {
								p.sendMessage(ChatColor.GOLD + "You're already fully revitalized!");
								return true;
							} // They have max HP.
							if(current_hp <= 0 || p.isDead() || !p.isOnline()){
								return true;
							}

							int amount_to_heal = (int) (current_hp / 2.25);

							if(PlayerManager.getPlayerModel(p).getToggleList() != null && PlayerManager.getPlayerModel(p).getToggleList().contains("debug")){
								p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "       +" + ChatColor.GREEN + (int) amount_to_heal + ChatColor.BOLD + " HP" + ChatColor.GRAY + " [" + ((int) current_hp + (int) amount_to_heal) + "/" + (int) max_hp + "HP]");
							}

							if((current_hp + amount_to_heal) >= max_hp) {
								p.setHealth(20);
								HealthMechanics.setPlayerHP(p.getName(), (int) max_hp);
								return true; // Full HP.
							}
							else if(p.getHealth() <= 19 && ((current_hp + amount_to_heal) < max_hp)) {
								HealthMechanics.setPlayerHP(p.getName(), (int) (HealthMechanics.getPlayerHP(p.getName()) + amount_to_heal));
								double health_percent = (HealthMechanics.getPlayerHP(p.getName()) + amount_to_heal) / max_hp;
								double new_health_display = health_percent * 20;
								if(new_health_display > 19) {
									if(health_percent >= 1) {
										new_health_display = 20;
									} else if(health_percent < 1) {
										new_health_display = 19;
									}
								}
								if(new_health_display < 1) {
									new_health_display = 1;
								}
								p.setHealth((int) new_health_display);

							}
							p.sendMessage(plugin.getMessage("healSelf"));
							phealPlayers.put(p.getUniqueId(), 35);
							return true;
						} else {
							sender.sendMessage(plugin.getMessage("waitHeal").replace("%TIME%", String.valueOf(phealPlayers.get(((Player) sender).getUniqueId()))));
							return true;
						}
					} else if (args.length == 1){
						if (!(phealOtherPlayers.containsKey(sender.getName()))){
							Player player = null;
							try {
								for (Player pa: Bukkit.getOnlinePlayers()){
									if (pa.getName().equals(args[0])){
										player = pa;
										break;
									} 
								}
								double max_hp = HealthMechanics.getMaxHealthValue(player.getName());
								double current_hp = HealthMechanics.getPlayerHP(player.getName());
								if(current_hp + 1 > max_hp) {
									p.sendMessage(ChatColor.GOLD + "This person is already fully revitalized!");
									return true;
								} // They have max HP.
								if(current_hp <= 0 || p.isDead() || !p.isOnline()){
									return true;
								}

								int amount_to_heal = (int) (current_hp / 2.5);

								if(PlayerManager.getPlayerModel(p).getToggleList() != null && PlayerManager.getPlayerModel(p).getToggleList().contains("debug")){
									p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "       +" + ChatColor.GREEN + (int) amount_to_heal + ChatColor.BOLD + " HP" + ChatColor.GREEN + " TO " + player.getName() + ChatColor.GRAY + " [" + ((int) current_hp + (int) amount_to_heal) + "/" + (int) max_hp + "HP]");
								}
								if(PlayerManager.getPlayerModel(player).getToggleList() != null && PlayerManager.getPlayerModel(player).getToggleList().contains("debug")){
									player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "       +" + ChatColor.GREEN + (int) amount_to_heal + ChatColor.BOLD + " HP" + ChatColor.GREEN + " FROM " + p.getName() + ChatColor.GRAY + " [" + ((int) current_hp + (int) amount_to_heal) + "/" + (int) max_hp + "HP]");
								}

								if((current_hp + amount_to_heal) >= max_hp) {
									player.setHealth(20);
									HealthMechanics.setPlayerHP(player.getName(), (int) max_hp);
									return true; // Full HP.
								}
								else if(player.getHealth() <= 19 && ((current_hp + amount_to_heal) < max_hp)) {
									HealthMechanics.setPlayerHP(player.getName(), (int) (HealthMechanics.getPlayerHP(player.getName()) + amount_to_heal));
									double health_percent = (HealthMechanics.getPlayerHP(player.getName()) + amount_to_heal) / max_hp;
									double new_health_display = health_percent * 20;
									if(new_health_display > 19) {
										if(health_percent >= 1) {
											new_health_display = 20;
										} else if(health_percent < 1) {
											new_health_display = 19;
										}
									}
									if(new_health_display < 1) {
										new_health_display = 1;
									}
									player.setHealth((int) new_health_display);

								}
								//player.setHealth(player.getMaxHealth());
								player.sendMessage(plugin.getMessage("healOther").replace("%PLAYER%", p.getName()));
								phealOtherPlayers.put(p.getUniqueId(), 240);
								return true;
							} catch (Exception ex){
								sender.sendMessage(plugin.getMessage("notOnline"));
								return true;
							}

						} else {
							int f = phealOtherPlayers.get(sender.getName());
							sender.sendMessage(plugin.getMessage("waitHeal").replace("%TIME%", String.valueOf(f)));
							return true;
						}
					} else {
						
						return true;
					}
				} else {
					p.sendMessage(plugin.getMessage("noRace").replace("%RACE%", "Priest"));
				}
			}
			return true;
		}
		return false;
	}
}
