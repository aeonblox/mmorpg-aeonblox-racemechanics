package com.aeonblox.RaceMechanics.commands;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;

public class PsmeltCommand implements CommandExecutor {
	RMMain plugin;
	public PsmeltCommand(RMMain plugin) {
		this.plugin = plugin;
	}
	public static ArrayList<UUID> psmeltPlayer = new ArrayList<UUID>();
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("psmelt")){
			if (sender instanceof Player){
				Player player = (Player) sender;
				if (plugin.utils.getPlayerRace(player.getUniqueId()) == RaceType.Demon){
					if (psmeltPlayer.contains(player.getUniqueId())){
						player.sendMessage(plugin.getMessage("psOff"));
						psmeltPlayer.remove(player.getUniqueId());
						return true;
					} else {
						player.sendMessage(plugin.getMessage("psOn"));
						psmeltPlayer.add(player.getUniqueId());
						return true;
					}
				} else {
					sender.sendMessage(ChatColor.RED + "You can't use this command, as you are no demon!");
					return true;
				}
			}
		}
		return false;
	}
}
