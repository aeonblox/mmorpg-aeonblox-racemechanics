package com.aeonblox.RaceMechanics.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;

public class PFlyCommand implements CommandExecutor{
	RMMain plugin;
	public PFlyCommand(RMMain plugin){
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("pfly")){
			if (sender instanceof Player){
				Player p = (Player) sender;
				if (plugin.utils.hasBonusRace(p.getUniqueId())){
					if (plugin.utils.getBonusRace(p.getUniqueId()) == BonusRaceType.Angel || plugin.utils.getPlayerRace(p.getUniqueId()) == RaceType.Titan){
						if (p.getAllowFlight()){
							p.setAllowFlight(false);
							p.setFlying(false);
							p.sendMessage(plugin.getMessage("flyOff"));
							return true;
						} else {
							p.setAllowFlight(true);
							p.setFlying(true);
							p.sendMessage(plugin.getMessage("flyOff"));
							return true;
						}
					} else {
						sender.sendMessage(plugin.getMessage("noRace").replace("%RACE%", "Angel"));
						return true;
					}
				} else {
					sender.sendMessage(plugin.getMessage("noRace").replace("%RACE%", "Angel or Titan"));
					return true;
				}
			}
			return true;
		}
		return false;
	}
}
