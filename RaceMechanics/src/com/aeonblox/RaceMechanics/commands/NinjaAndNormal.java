package com.aeonblox.RaceMechanics.commands;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.BonusRaceType;
import com.aeonblox.RaceMechanics.RMMain;

public class NinjaAndNormal implements CommandExecutor{
	RMMain plugin;
	public NinjaAndNormal(RMMain instance){
		this.plugin = instance;
	}
	ArrayList<UUID> hiddenPlayers = new ArrayList<UUID>();
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("normal")){
			if (sender instanceof Player){
				Player player = (Player) sender;
				if (plugin.utils.getBonusRace(player.getUniqueId()) == BonusRaceType.Assassin || player.hasPermission("aeonrpg.admin")){
					if (!hiddenPlayers.contains(player.getUniqueId())){
						hiddenPlayers.add(player.getUniqueId());
						for (Player p: Bukkit.getOnlinePlayers()){
							p.hidePlayer(player);
						}
						player.sendMessage(plugin.getMessage("nowHidden"));
						return true;
					} else {
						hiddenPlayers.remove(player.getUniqueId());
						for (Player p: Bukkit.getOnlinePlayers()){
							p.showPlayer(player);
						}
						player.sendMessage(plugin.getMessage("unHidden"));
						return true;
					}
				} else {
					player.sendMessage(plugin.getMessage("noAssassin").replace("%RACE%", "Assassin"));
					return true;
				}
			}
		}
		return false;
	}
}
