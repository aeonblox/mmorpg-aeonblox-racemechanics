package com.aeonblox.RaceMechanics.commands;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.RaceType;

public class PBrightCommand implements CommandExecutor{
	RMMain plugin;
	public PBrightCommand(RMMain plugin){
		this.plugin = plugin;
	}
	public static ArrayList<UUID> fullbright = new ArrayList<UUID>();
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("pbright")){
			if (sender instanceof Player){
				Player player = (Player) sender;
				if (plugin.utils.getPlayerRace(player.getUniqueId()) == RaceType.Wraith){
					if (!fullbright.contains(player.getUniqueId())){
						player.sendMessage(plugin.getMessage("nvOn"));
						player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 1, Integer.MAX_VALUE));
						fullbright.add(player.getUniqueId());
						return true;
					} else {
						player.sendMessage(plugin.getMessage("nvOff"));
						player.removePotionEffect(PotionEffectType.NIGHT_VISION);
						fullbright.remove(player.getUniqueId());
						return true;
					}
				} else {
					sender.sendMessage(plugin.getMessage("noRace").replace("%RACE%", "Wraith"));
					return true;
				}
			}
			return true;
		}
		return false;
	}
	public static void revertFullbright(){
		for (UUID u: fullbright){
			Bukkit.getPlayer(u).removePotionEffect(PotionEffectType.NIGHT_VISION);
		}
	}
}
