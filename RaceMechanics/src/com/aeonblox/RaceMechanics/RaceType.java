package com.aeonblox.RaceMechanics;

import java.util.HashMap;

import org.bukkit.ChatColor;

public enum RaceType {
	Default("default", ChatColor.GRAY+ ""),
	Demon("demon", ChatColor.RED+ ""),
	Elf("elf", ChatColor.GREEN+ ""), 
	Nymph("nymph", ChatColor.AQUA+ ""),
	Chameleon("chameleon", ChatColor.DARK_PURPLE+ ""),
	Priest("priest", ChatColor.YELLOW+ ""),
	Wraith("wraith", ChatColor.BLACK+ ""),
	Titan("titan", ChatColor.GOLD + ""),
	Human("human", ChatColor.LIGHT_PURPLE + ""),;
	private String name;
	private String nameColor;
	private RaceType(String name, String color){
		this.name = name;
		this.nameColor = color;
	}
	public String getNameColor(){
		return nameColor;
	}
	public String getName(){
		return this.name;
	}
	private static HashMap<String, RaceType> BY_NAME = new HashMap<String,RaceType>();
	private static HashMap<String, BonusRaceType> BY_NAME_BONUS = new HashMap<String,BonusRaceType>();
	public static RaceType forName(String name){
		RaceType type = BY_NAME.get(name.toLowerCase());
		if (type == null){
			return RaceType.Default;
		} else {
			return type;
		}
	}
	public static void putTypesInThing(){
		for (RaceType type: values()){
			BY_NAME.put(type.name, type);
		}
		for (BonusRaceType b :BonusRaceType.values()){
			BY_NAME_BONUS.put(b.getName(), b);
		}
		for (TitanType t: TitanType.values()){
			TitanType.array.put(t.getName(), t);
		}
	}
	public static BonusRaceType bonusForName(String s){
		if (!(s == null)){
			BonusRaceType type = BY_NAME_BONUS.get(s.toLowerCase());
			if (type == null){
				return BonusRaceType.None;
			}
			return type;
		} else {
			return BonusRaceType.None;
		}
	}
	public static RaceType forNameWithNPE(String string) {
		return BY_NAME.get(string);
	}
}