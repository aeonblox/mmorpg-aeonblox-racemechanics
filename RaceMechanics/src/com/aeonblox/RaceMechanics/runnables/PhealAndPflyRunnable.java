package com.aeonblox.RaceMechanics.runnables;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.aeonblox.RaceMechanics.RMMain;
import com.aeonblox.RaceMechanics.commands.PhealCommand;

public class PhealAndPflyRunnable implements Runnable {
	private RMMain plugin;
	public PhealAndPflyRunnable(RMMain instance) {
		plugin = instance;
	}
	public void run(){
		for (UUID in :PhealCommand.phealPlayers.keySet()){
			int inte = PhealCommand.phealPlayers.get(in);
			inte--;
			PhealCommand.phealPlayers.remove(in);
			if (inte != 0){
				PhealCommand.phealPlayers.put(in, inte);
			} 
		}
		for (UUID in :PhealCommand.phealOtherPlayers.keySet()){
			int inte = PhealCommand.phealOtherPlayers.get(in);
			inte--;
			PhealCommand.phealOtherPlayers.remove(in);
			if (inte != 0){
				PhealCommand.phealOtherPlayers.put(in, inte);
			} 
		}
		for (final Player p: Bukkit.getOnlinePlayers()){
			if (p.isFlying() && !p.hasPermission("RaceMechanics.flybypass")){
				if (p.getLocation().getY() - p.getWorld().getHighestBlockAt(p.getLocation()).getY() > 10){
					p.sendMessage(plugin.getMessage("noHigher"));
					p.setAllowFlight(false);
					p.setFlying(false);
					Bukkit.getScheduler().runTaskLater(plugin, new Runnable(){
						public void run(){
							p.setAllowFlight(true);
							p.setFlying(true);
						}
					}, 10L);
				}
			}
		}
	}
}
