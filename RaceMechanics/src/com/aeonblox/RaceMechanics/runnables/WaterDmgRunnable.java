package com.aeonblox.RaceMechanics.runnables;

import com.aeonblox.rpg.HealthMechanics.HealthMechanics;
import com.aeonblox.rpg.managers.PlayerManager;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.RaceMechanics.RaceType;
import com.aeonblox.RaceMechanics.utils.Utils;

public class WaterDmgRunnable implements Runnable {
	private Utils utils;
	public WaterDmgRunnable(Utils utils){
		this.utils = utils;
	}
	public ArrayList<UUID> hasBoost = new ArrayList<UUID>();
	@Override
	public void run(){
		for (Player p: Bukkit.getOnlinePlayers()){
			if (utils.getPlayerRace(p.getUniqueId()) == RaceType.Demon){
				Player player = p;
				Material m = player.getLocation().getBlock().getType();
				if (m == Material.STATIONARY_WATER || m == Material.WATER){
					if (!player.isDead()){
						if(!(HealthMechanics.in_combat.containsKey(player.getName()))) {
							HealthMechanics.in_combat.put(p.getName(), System.currentTimeMillis());
						}

						double max_hp = HealthMechanics.getMaxHealthValue(p.getName());
						double total_hp = HealthMechanics.getPlayerHP(p.getName());
						double damage = total_hp * 0.2D;

						double new_hp = total_hp - damage;
						HealthMechanics.setPlayerHP(p.getName(), (int) new_hp);
						//player.damage(player.getHealth() * 0.2D);

						if (new_hp <= 1) {
							PlayerManager.getPlayerModel(p).setDeathLocation(p.getLocation());
							p.setSneaking(false);
							p.setHealth(0);
							HealthMechanics.setPlayerHP(p.getName(), 0);
							return;
						}
						p.sendMessage(ChatColor.RED + "Gaaaaaarh! Accursed water, it burns! I need to get out!");
						//if (player.getMaxHealth() - player.getHealth() > 1){
						//	HealthMechanics.setPlayerHP(player.getName(), (int) (player.getHealth() + 1));
						//}
						//player.damage(player.getHealth() * 0.15D);
					}
				} else if (player.getWorld().hasStorm()){
					if (player.getWorld().getHighestBlockAt(player.getLocation()).getY() < player.getLocation().getY() + 2){
						if (!player.isDead()){
							if(!(HealthMechanics.in_combat.containsKey(player.getName()))) {
								HealthMechanics.in_combat.put(p.getName(), System.currentTimeMillis());
							}

							double max_hp = HealthMechanics.getMaxHealthValue(p.getName());
							double total_hp = HealthMechanics.getPlayerHP(p.getName());
							double damage = total_hp * 0.2D;

							double new_hp = total_hp - damage;
							HealthMechanics.setPlayerHP(p.getName(), (int) new_hp);
							player.damage(player.getHealth() * 0.2D);
							if (new_hp <= 1) {
								PlayerManager.getPlayerModel(p).setDeathLocation(p.getLocation());
								p.setSneaking(false);
								p.setHealth(0);
								HealthMechanics.setPlayerHP(p.getName(), 0);
								return;
							}

							p.sendMessage(ChatColor.RED + "Ouuuuuch! The Rain it hurts! You need to find shelter quickly!");
							//if (player.getMaxHealth() - player.getHealth() > 1){
							//	HealthMechanics.setPlayerHP(player.getName(), (int) (player.getHealth() + 1));
							//}
						}
					}
				}
			} else if (utils.getPlayerRace(p.getUniqueId()) == RaceType.Nymph) {
				if (p.getWorld().hasStorm()){
					if (p.getWorld().getHighestBlockAt(p.getLocation()).getY() < p.getLocation().getY() + 2){
						if (!p.isDead()) {
							p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 4, 1));
						}
					}
				}
			}
		}
	}
}
